import defaultTheme from 'tailwindcss/defaultTheme';
// import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['NotoSans', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'primary': '#F58233',
                'secondary': '#606466'
            },
            spacing: {
                header: '3.5rem',
                'header-xl': '4.4rem',
                main: 'calc(100vh - 3rem)',
                footer: '3rem',
                '82': '22rem',
                '98': '26rem',
                '100': '28rem'
            },
            screens: {
                '3xl': '1920px',
                '4xl': '3840px',
                '5xl': '5120px',
                'hsm': { 'raw': '(max-height: 700px) and (min-width: 920px)' }
            },
            borderWidth: {
                '3': '3px'
            },
            zIndex: {
                top: '9999999',
                bottom: '-9999999'
            },
            aspectRatio: {
                'image': '16 / 12',
            },
            boxShadow: {
                b: '0px 5px 7px -5px rgba(34, 60, 80, 0.28)',
                t: '0px -5px 7px -5px rgba(34, 60, 80, 0.28)'
            },
            backgroundSize: {
                loader: '800px 104px'
            },
            keyframes: {
                loader: {
                    '0%': {
                        backgroundPosition: -468
                    },
                    '50%': {
                        backgroundPosition: 0
                    },
                    '100%': {
                        backgroundPosition: 468
                    },
            }
            },
            animation: {
                loader: 'loader 3s linear infinite',
             }
        },
    },

    // plugins: [forms],
};
