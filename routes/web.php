<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use  App\Http\Controllers\MainPageController;
use  App\Http\Controllers\ArticlesController;
use  App\Http\Controllers\SupportController;
use  App\Http\Controllers\ContactsController;
use  App\Http\Controllers\AboutController;
use  App\Http\Controllers\ProductController;

Route::get('/', [MainPageController::class, 'index'])->name('page.main');

Route::group(['prefix'=> 'products'], function () {
    Route::get('/', [ProductController::class, 'index'])->name('page.products.index');
    Route::get('/{item?}', [ProductController::class, 'index'])->name('page.products.detail');
});

Route::get('/support', [SupportController::class, 'index'])->name('page.support');
Route::get('/about', [AboutController::class, 'index'])->name('page.about');
Route::get('/contacts', [ContactsController::class, 'index'])->name('page.contacts');

Route::get('/articles/', [ArticlesController::class, 'index'])->name('page.articles');
Route::get('/articles/{category}', [ArticlesController::class, 'index'])->name('page.articles.category');
Route::get('/articles/{category}/{article}', [ArticlesController::class, 'index'])->name('page.articles.detail');

Route::inertia('/policy/pers', 'Policy/Pers');
Route::inertia('/policy/cookies', 'Policy/Cookies');

// Route::get('/policy/pers', Inertia::render('/Policy/Pers', 'Policy Personal'))->name('page.policy.pers');

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
