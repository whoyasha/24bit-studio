<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use  App\Http\Controllers\NewsLineController;
use  App\Http\Controllers\ArticlesApiController;
use  App\Http\Handlers\TnvedHandler;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix'=> 'v1'], function () {
    Route::get('/info', [NewsLineController::class, 'index'])->name('info.api');
    Route::get('/articles', [ArticlesApiController::class, 'index'])->name('articles.api');
    Route::get('/products', [AProductsController::class, 'index'])->name('products.api');
    Route::get('/corporate', [AProductsController::class, 'index'])->name('corporate.api');
    Route::get('/tnved', [TnvedHandler::class, 'index'])->name('tnved.api');
    Route::post('/support', [SupportController::class, 'store'])->name('support.api');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
