document.addEventListener("DOMContentLoaded",() => {
	// let form = new Form('faq');
});

class Form {

	constructor( name ) {

		this.errors = {};
		this.errors_mess = {
			required : 'Не заполнено поле ',
			wrong    : 'Не верно заполено поле '
		}

		<!-- this.name = name; -->
		this.isAjax = false;
		this.validate = [];
		<!-- this.form = document.querySelector('[name="' + name + '"]'); -->
		<!-- this.container = this.form.parentNode; -->
		this.successMessage = this.form.querySelector('[data-mess="success"]');
		this.errorsMessage = this.form.querySelector('[data-mess="errors"]');
		this.description = this.form.querySelector('[data-mess="description"]');
		this.url = this.form.action;
		this.btn = this.form.querySelector('[type="submit"]');
		this.fields = this.formData();
		this.defaultMin = 2;

		if ( this.validate.length )
			this.checker = new FormChecker(this);

		this.onLoad();
		this.onInput();

		const __this = this;

		this.form.onsubmit = (e) => {

			e.preventDefault();

			this.validate.forEach( function( field ) {
				__this.checker.check(field);
			});

			if ( this.isEmptyErrors() && this.isAjax )
				this.sendRequest();
			else
				this.form.submit;
		}
	}

	clear = function(fields = false) {
		this.successMessage.style.display = 'none';
		this.errorsMessage.style.display = 'none';
		this.errorsMessage.innerHTML = '';
		this.errors = {};
		this.fields.forEach( function( field ) {
			field.data.fieldset.classList.remove('inp--error');

			/**
			 * Clear fields values
			 *
			if ( fields ) {

			}
			/**/
		});
	}

	onLoad = function(field){

		const __this = this;

		this.fields.forEach( function( field ) {
			__this.updField(field);
		});
	}

	onInput = function() {

		const __this = this;

		this.fields.forEach( function( field ) {
			field.oninput = (e) => {
				__this.updField(field, true);
			}
		});
	}

	updField = function( field, check = false ) {

		if ( field.data.toggle )
			this.toggle(field);

		if ( field.data.resize )
			field.data.resize.exec();

		if ( check && null !== field.data.validate )
			this.checker.check(field);
	}

	sendRequest = async function(e) {
		const text = this.btn.value;
		this.btn.value = 'Отправляем...';

		let data = this.formData(false);

		let response = await fetch(this.url, {
			method: 'POST',
				body: data
			});

		let result = await response.json();
		this.btn.value = text;

		if ( result.SUCCESS > 0 ) {
			this.successMessage.style.display = 'block';
			this.errorsMessage.style.display = 'none';
		}

		if ( result.ERRORS ) {
			this.successMessage.style.display = 'none';
			this.errorsMessage.style.display = 'block';
			this.errorsMessage.innerHTML = result.ERRORS;
		}

		this.description.style.display = 'none';
	}

	prepareFields = function( field ) {

		let item = field;
		let tag  = field.tagName.toLowerCase();
		let name = field.getAttribute('name');
		let key  = name.match(/(\[?(\d|\w+)\])/);
		let min  = field.getAttribute('data-min');

		item['data']     = {
			label     : this.getLabel(field),
			validate : field.getAttribute('data-validate'),
			type      : tag == 'select' || tag == 'textarea' ? tag : field.getAttribute('type'),
			name      : name.replace(/(\[?(\d|\w+)\])|(\[\])/, ''),
			multiple  : /(\[?(\d|\w+)\])|(\[\])/.test(field.getAttribute('name')),
			fieldset  : field.parentNode,
			min       : null !== min ? Number(min) : this.defaultMin,
			resize    : null,
			toggle    : false,
			hidden : field.dataset.hidden === 'y'
		}

		if ( item.data.hidden ) {
			item.data.fieldset.style.display = 'none';
		}

		if ( item.data.type == 'checkbox' || item.data.type == 'radio' ) {
			item.data.fieldset = field.parentNode.parentNode;
			item.data.toggle = true;
		}

		if ( field.data.type == 'textarea' )
			item.data.resize = new TextareaAutoResize(item);

		if ( item.data.multiple ) {

			let fieldset = field.parentNode.parentNode;
				min = fieldset.getAttribute('data-min');

			if ( fieldset.tagName.toLowerCase() == 'fieldset' || utils.hasClass(fieldset, 'inp__fieldset') ) {
				item.data['fieldset'] = fieldset.parentNode;
				item.data['fields']   = fieldset.querySelectorAll('input');
				item.data['validate'] = fieldset.getAttribute('data-validate');
				item.data['min']      = null !== min ? Number(min) : this.defaultMin;
			}
		}

		if ( null !== item.data.validate )
			this.validate.push(item);

		return item;
	}

	formData = function( set = true ) {

		const __this = this;

		let fields = [];
		let data = new FormData(this.form);

		if ( !set )
			return data;

		for ( let field of data ) {
			let obj = __this.form.querySelector('[name="' + field[0] + '"]');
			fields.push(__this.prepareFields(obj));

			if ( field[0] == 'is_ajax' && field[1] == 'Y' )
				this.isAjax = true;
		}

		let chckbxs = this.addFields('checkbox');
			if ( chckbxs.length )
				fields = fields.concat(chckbxs);

		let radio = this.addFields('radio');
			if ( radio.length )
				fields = fields.concat(radio);

		return fields;
	}

	setDefaultValue = function(fieldName, value) {
		let field = this.form.querySelector('[name="' + fieldName + '"]');
		field.value = value;
	}

	addFields = function( type ) {

		const __this = this;

		let result = [];
		let checkboxes = __this.form.querySelectorAll('[type="' + type + '"]');

		checkboxes.forEach( function(obj) {
			result.push(__this.prepareFields(obj));
		});

		return result;
	}

	getLabel = function( field ) {
		let parent = field.parentNode;
		return ( parent.tagName == 'LABEL' )
			? parent.innerText.trim()
				.replace(':', '')
				.replace('&nbsp;', '')
			: null;
	}

	toggle = function( field ) {
		let toggle = this['toggle' + utils.capitalize(field.data.type)];

		if ( toggle !== undefined )
			toggle.apply(this, [field]);
	}

	toggleCheckbox = function( field ){
		field.checked
			? field.parentNode.classList.add('label--checked')
			: field.parentNode.classList.remove('label--checked');
	}

	toggleRadio = function( field ){

		let name = field.getAttribute('name');

		this.fields.forEach( function(item) {
			if ( item.getAttribute('name') == name )
				item.parentNode.classList.remove('label--checked');
		});

		if ( !utils.hasClass(field.parentNode, 'label--clear') && field.checked )
			field.parentNode.classList.add('label--checked');
	}

	check = function( field ) {

		let checker = new FormChecker();
		checker.exec(field);

		return false;

		const __this = this;

		let check = false;
		let name = field.getAttribute('name');
		let value = field.value;

		if ( null !== field.data.validate ) {
			let fnName = 'check' + field.data.validate.charAt(0).toUpperCase() + field.data.validate.slice(1);
			let fn = __this[fnName];

			if ( fn !== undefined )
				check = fn.apply(__this, [field, value]);
		}

		let messKey = field.data.validate == 'required'
			? 'required'
			: 'wrong';

		let mess = this.errors_mess[messKey] + '"' + field.data.label + '"';

		if ( !check ) {
			__this.errors[name] = mess;
			field.data.fieldset.classList.add('inp--error');
		} else {
			field.data.fieldset.classList.remove('inp--error');
			delete __this.errors[name];
		}
	}

	isEmptyErrors = function() {
		return Object.keys(this.checker.getErrors()).length === 0;
	}

	checkEmail = function( field, value ) {

		let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([а-яёА_ЯЁa-zA-Z\-0-9]+\.)+[а-яёА_Яa-zA-Z]{2,}))$/;

		return pattern.test(value);
	}

	checkRequired = function( field, value ) {

		const __this = this;

		switch ( field.data.type ) {

			case 'text' :
			case 'textarea' :
				if ( !field.data.multiple ) {
					return value.length >= field.data.min;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( item.value.length < field.data.min )
							empty = Math.ceil(empty+1);
					});

					return empty !== field.data.fields.length;
				}
				break;

			case 'select' :
				return value.length > 0;
				break;

			case 'radio' :
			case 'checkbox' :
				if ( !field.data.multiple ) {
					return field.checked;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( !item.checked && item.value.length )
							empty = Math.ceil(empty+1);
					});
					return empty !== field.data.fields.length;
				}
				break;
		}
	}
}

const utils = {
	sum : function( array ) {
		return array.reduce((partialSum, a) => partialSum + a, 0);
	},
	capitalize : function( str ) {
		return str.split('')[0].toUpperCase() + str.slice(1);
	},
	hasClass : function( field, className ) {
		return (" " + field.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1;
	}
}

class FormChecker {

	constructor( form ) {

		this.errors = {};
		this.defaultMin = 3;
		this.errors_mess = {
			required : 'Не заполнено поле ',
			wrong    : 'Не верно заполено поле '
		}

		this.form = form;
	}

	check = function( field ) {

		let check = false;
		let name = field.getAttribute('name');
		let value = field.value;

		if ( null !== field.data.validate ) {
			let fnName = 'check' + field.data.validate.charAt(0).toUpperCase() + field.data.validate.slice(1);
			let fn = this[fnName];

			if ( fn !== undefined )
				check = fn.apply(this, [field, value]);
		}

		let messKey = field.data.validate == 'required'
			? 'required'
			: 'wrong';

		let mess = this.errors_mess[messKey] + '"' + field.data.label + '"';

		if ( !check ) {
			this.errors[name] = mess;
			field.data.fieldset.classList.add('inp--error');
		} else {
			field.data.fieldset.classList.remove('inp--error');
			delete this.errors[name];
		}
	}

	checkPhone = function( field, value ) {
		let pattern = /(^8|7|\+7)((\d{10})|(\s\(\d{3}\)\s\d{3}\s\d{2}\s\d{2}))/;
		return pattern.test(value);
	}

	checkEmail = function( field, value ) {

		let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([а-яёА_ЯЁa-zA-Z\-0-9]+\.)+[а-яёА_Яa-zA-Z]{2,}))$/;

		return pattern.test(value);
	}

	checkRequired = function( field, value ) {

		switch ( field.data.type ) {

			case 'text' :
			case 'textarea' :
				if ( !field.data.multiple ) {
					if ( field.data.min === undefined )
						field.data.min = this.defaultMin;

					return value.length >= field.data.min;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( item.value.length < field.data.min )
							empty = Math.ceil(empty+1);
					});

					return empty !== field.data.fields.length;
				}
				break;

			case 'select' :
				return value.length > 0;
				break;

			case 'radio' :
			case 'checkbox' :
				if ( !field.data.multiple ) {
					return field.checked;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( !item.checked && item.value.length )
							empty = Math.ceil(empty+1);
					});
					return empty !== field.data.fields.length;
				}
				break;
		}
	}

	getErrors = function() {
		return this.errors;
	}
}

class TextareaAutoResize {

	constructor ( field ) {
		this.field = field;
		this.defaultHeight = 34;
		this.result = 0;
		this.strCnt = 0;
		this.lenArray = [];
		this.sum = 0;
		this.lastStr = 0;

		this.field.onkeypress = (e) => {
			let keyCode = e.which;
			if ( keyCode == 13 )
				e.preventDefault();
		}
	}

	exec = function() {

		if ( !this.field.value.length )
			this.result = this.defaultHeight;
		else
			this.result = this.field.scrollHeight > this.defaultHeight
				? this.field.scrollHeight
				: this.defaultHeight;

		this.field.style.height = this.result + 'px';
		this.field.style.minHeight = this.defaultHeight + 'px';
		this.field.style.overflow = 'hidden';

		/* let cnt = Math.ceil(this.field.scrollHeight/this.defaultHeight);

		if ( cnt > this.strCnt && this.field.value.length > this.lastStrCnt ) {
			this.strCnt = cnt;
			this.lenArray.push(this.field.value.length);
			this.lastStr = this.lenArray[this.lenArray.length-1];

			console.log('this.lenArray in : ', this.lenArray, this.lastStr, this.field.value.length);
		}

		if ( this.field.value.length < this.lastStrCnt ) {

			this.lenArray.splice(this.lenArray.length-1);
			this.strCnt = this.strCnt-1;
			this.lastStr = this.lenArray[this.lenArray.length-1];
		} */
	}
}
