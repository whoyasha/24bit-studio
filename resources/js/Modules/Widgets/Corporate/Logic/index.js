import Main from '@/Modules/Widgets/Corporate/Logic/Main.vue';
import Slider from '@/Modules/Widgets/Corporate/Logic/Slider.vue';

export default {
    main: Main,
    slider: Slider
}
