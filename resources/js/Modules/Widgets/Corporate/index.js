import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Corporate/Logic';
import Ui from '@/Modules/Widgets/Corporate/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Widgets/Corporate/Ui/icons.js';

import { reactive, toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';
import { useFormController } from '@/Modules/Forms';
import axios from '@/Modules/DataPorviders/axios';

export function useCorporate() {

    const corporate = reactive(new Module());

    /**
     * Config module
     */
    corporate.logic(Logic)
            .ui('ui', Ui)
            .ui('main.ui', MainUi)
            .ui('icons', Icons);

    const data = toRaw(usePage().props.pageData);
    const controller = useFormController(
        'corporate',//id
        data.corporate_form.fields,
        'default'//checker
    );

    controller.instance(
        'data-provider',
        axios,
        [
            controller,
            'corporate.api',//form action
            'post'
        ]
    );
    controller.set('button-title', 'Отправить заявку')
            .set('color', 'success')
            .set('form-action', 'corporate.api')
            .set('userconsent', true)
            .set('placeholder', true)
            .set('show-label', 0);

    controller.set('show-cancel-button', 'Очистить форму');
    controller.set('method', 'post');

    controller.set('callback:success.mess', 'Завка успешно отправлена.');

    corporate.use('controller', controller);

    return corporate;
}
