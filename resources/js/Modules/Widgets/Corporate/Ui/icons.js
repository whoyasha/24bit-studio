import {
    IconBrandWhatsapp,
    IconMessageFilled,

    IconInfoCircle,
    IconTerminal2,
    IconLicense,
    IconHeadset
} from '@tabler/icons-vue';

export default {
    wa: IconBrandWhatsapp,
    message: IconMessageFilled,

    consulting: IconInfoCircle,
    dev: IconTerminal2,
    license: IconLicense,
    support: IconHeadset
}
