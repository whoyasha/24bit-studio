import Wrapper from '@/Modules/Widgets/Corporate/Ui/Wrapper.vue';
import Inner from '@/Modules/Widgets/Corporate/Ui/Inner.vue';

import Left from '@/Modules/Widgets/Corporate/Ui/Slider/Left.vue';
import Slider from '@/Modules/Widgets/Corporate/Ui/Slider/Slider.vue';
import Message from '@/Modules/Widgets/Corporate/Ui/Slider/Message.vue';

import Right from '@/Modules/Widgets/Corporate/Ui/Right.vue';

export default {
    'wrapper': Wrapper,
    'inner': Inner,

    'slider.left': Left,
    'slider.slider': Slider,
    'slider.message': Message,

    'right': Right,
}
