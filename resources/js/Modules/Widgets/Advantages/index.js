import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Advantages/Logic';
import Ui from '@/Modules/Widgets/Advantages/Ui';
import MainUi from '@/Modules/Main/Ui';

let advantages = null;

export function useAdvantages() {
    if ( !advantages ) {
        advantages = new Module();

        /**
         * Config module
         */
        advantages.logic(Logic)
                .ui('ui', Ui)
                .ui('main.ui', MainUi);

        const items = [
            {
                code: 'concept',
                image: '/images/tabler/dark/project.svg',
                description: 'Тщательно вникаем в проект, прорабатываем концепцию'
            },
            {
                code: 'bp',
                image: '/images/tabler/dark/archive.svg',
                description: 'Прозрачность ценообразования и бизнес-процессов'
            },
            {
                code: 'result',
                image: '/images/tabler/dark/dart.svg',
                description: 'Ориенти рованность на результат. В приоритете решение задачи'
            },
            {
                code: 'react',
                image: '/images/tabler/dark/shield.svg',
                description: 'Быстрое реагирование в случае сбоя. Всегда на связи'
            },
            {
                code: 'individual',
                image: '/images/tabler/dark/boy-gives-flowers.svg',
                description: 'Индивидуальный подход для каждого клиента'
            },
            {
                code: 'nonstandart',
                image: '/images/tabler/dark/building.svg',
                description: 'Не боимся разрабатывать нестандартные решения'
            },

        ];

        advantages.set('data:items', items);
    }
    return advantages;
}
