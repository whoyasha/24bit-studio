import Main from '@/Modules/Widgets/Advantages/Logic/Main.vue';
import Item from '@/Modules/Widgets/Advantages/Logic/Item.vue';

export default {
    main: Main,
    item: Item
}
