import Wrapper from '@/Modules/Widgets/Advantages/Ui/Wrapper.vue';
import Item from '@/Modules/Widgets/Advantages/Ui/Item.vue';

export default {
    'wrapper': Wrapper,
    'item': Item
}
