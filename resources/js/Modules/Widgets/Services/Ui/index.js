import Wrapper from '@/Modules/Widgets/Services/Ui/Wrapper.vue';
import Loader from '@/Modules/Widgets/Services/Ui/Loader.vue';

import ResultContainer from '@/Modules/Widgets/Services/Ui/Result/Container.vue';
import ResultWrapper from '@/Modules/Widgets/Services/Ui/Result/Wrapper.vue';
import ResultContent from '@/Modules/Widgets/Services/Ui/Result/Content.vue';
import ResultItem from '@/Modules/Widgets/Services/Ui/Result/Item.vue';
import ResultItemDescription from '@/Modules/Widgets/Services/Ui/Result/ItemDescription.vue';
import ResultHead from '@/Modules/Widgets/Services/Ui/Result/Head.vue';
import ResultTop from '@/Modules/Widgets/Services/Ui/Result/Top.vue';

export default {
    'wrapper': Wrapper,
    'loader': Loader,

    'result.container': ResultContainer,
    'result.wrapper': ResultWrapper,
    'result.content': ResultContent,
    'result.head': ResultHead,
    'result.item': ResultItem,
    'result.item-description': ResultItemDescription,
    'result.top': ResultTop,
}
