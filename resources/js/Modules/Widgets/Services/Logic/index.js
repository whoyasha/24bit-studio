import Main from '@/Modules/Widgets/Services/Logic/Main.vue';
import Result from '@/Modules/Widgets/Services/Logic/Result.vue';
export default {
    main: Main,
    result: Result
}
