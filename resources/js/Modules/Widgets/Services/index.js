import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Services/Logic';
import Ui from '@/Modules/Widgets/Services/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import { useFormController } from '@/Modules/Forms';
import axios from '@/Modules/DataPorviders/axios';

let tnvedInstance = null;

const tnvedConfig = ( instance ) => {

    console.log('axios in : ', axios);

    instance.logic(Logic)
         .ui('ui', Ui)
         .ui('main.ui', MainUi)
         .ui('icons', Icons);

    const fields = [
        {
            code: 'tnved',
            form_type: "tnved",
            placeholder: 'начните вводить код',
            autofocus: false,
            required: true,
            /**
             * length - obj with keys min, max or number
             * numeric value - obj with keys min, max or number
             */
            validate: {
                length: {min: 4, max: 10}
            }
            /**/
        },
    ];

    /** Form */
    const form = useFormController(
        'tnved',//id
        fields,
        'default'//checker,
    );
    form.instance(
        'data-provider',
        axios,
        [
            form,//current form controller
            'tnved.api',//form action route
            'get'//method
        ]
    );
    form.set('callback:provider.success', 'external')
            .set('callback:provider.error', 'notify')
            .set('button-title', 'Проверить')
            .set('userconsent', false)
            .set('color', 'violet')
            .set('button-size', 'sm');

    instance.use('controller', form);
    /** Form */
}

export function useServices() {

    if ( !tnvedInstance ) {
        tnvedInstance = new Module();
        tnvedConfig(tnvedInstance);
    }

    return tnvedInstance;
}
