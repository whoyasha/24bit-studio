import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Contacts/Logic';
import Ui from '@/Modules/Widgets/Contacts/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Widgets/Contacts/Ui/icons.js';
import { usePage } from '@inertiajs/vue3';

let contacts = null;

export function useContacts() {
    if ( !contacts ) {
        contacts = new Module();

        /**
         * Config module
         */
        contacts.logic(Logic)
                .ui('ui', Ui)
                .ui('main.ui', MainUi)
                .ui('icons', Icons);

        contacts.set('data', usePage().props.pageData.contacts);
    }
    return contacts;
}
