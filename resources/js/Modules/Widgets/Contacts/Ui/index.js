import Wrapper from '@/Modules/Widgets/Contacts/Ui/Wrapper.vue';
import Item from '@/Modules/Widgets/Contacts/Ui/Item.vue';
export default {
    'wrapper': Wrapper,
    'item': Item
}
