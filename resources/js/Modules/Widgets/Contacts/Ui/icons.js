import {
    IconPhone,
    IconAt
 } from '@tabler/icons-vue';

export default {
    phone: IconPhone,
    email: IconAt,
}
