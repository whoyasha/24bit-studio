import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Banners/Logic';
import Ui from '@/Modules/Widgets/Banners/Ui';
import MainUi from '@/Modules/Main/Ui';

import { toRaw } from 'vue';
// import { usePage } from '@inertiajs/vue3';
import { useSlider } from '@/Modules/Slider';

import { useScrollTo } from '@/Modules/Main/Ui/useScrollTo.js';

import { usePageData } from '@/Modules/PageData';

let bannersInstance = null;

export function useBanners() {
    // if ( !bannersInstance ) {
        bannersInstance = new Module();

        /**
         * Config module
         */
        bannersInstance.logic(Logic)
                .ui('ui', Ui)
                .ui('main.ui', MainUi);

        bannersInstance.use('slider', useSlider);
        bannersInstance.use('scrollTo', useScrollTo);

        const PageData = usePageData();

        bannersInstance.use('pageData', usePageData);

        const banners = PageData.get('data:pageData.banners');

        if ( banners.length ) {
            bannersInstance.set('data', toRaw(banners));
            bannersInstance.states({
                activeBannerColor: bannersInstance.get('data:0.color')
            });
        }
    // }
    return bannersInstance;
}
