import Slide from '@/Modules/Widgets/Banners/Ui/Slide.vue';
import Wrapper from '@/Modules/Widgets/Banners/Ui/Wrapper.vue';
import CardWrapper from '@/Modules/Widgets/Banners/Ui/CardWrapper.vue';

import Inner from '@/Modules/Widgets/Banners/Ui/Inner.vue';

export default {
    'slide': Slide,
    'wrapper': Wrapper,
    'card-wrapper': CardWrapper,
    'inner': Inner,
}
