import Wrapper from '@/Modules/Widgets/Info/Ui/Wrapper.vue';

import Head from '@/Modules/Widgets/Info/Ui/Head/Main.vue';
import HeadFilter from '@/Modules/Widgets/Info/Ui/Head/Filter.vue';
import HeadFilterItem from '@/Modules/Widgets/Info/Ui/Head/FilterItem.vue';

import ItemWrapper from '@/Modules/Widgets/Info/Ui/Item/Wrapper.vue';
import Item from '@/Modules/Widgets/Info/Ui/Item/Main.vue';

import Preloader from '@/Modules/Widgets/Info/Ui/Preloader.vue';
import Foot from '@/Modules/Widgets/Info/Ui/Foot.vue';

import BodyMoreIcon from '@/Modules/Widgets/Info/Ui/Body/MoreIcon.vue';

export default {
    'wrapper': Wrapper,

    'head': Head,
    'head.filter': HeadFilter,
    'head.filter-item': HeadFilterItem,

    'body.more-icon': BodyMoreIcon,

    'item.wrapper': ItemWrapper,
    'item': Item,

    'preloader': Preloader,
    'foot': Foot
}
