import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Widgets/Info/Logic';
import Ui from '@/Modules/Widgets/Info/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

// import { useMain } from '@/Modules/Main';

import { useDataController } from '@/Modules/DataController';
import { useFilter } from '@/Modules/DataController/Filter';

import { toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';

let Info = null;

export function useInfo() {

    /**
     * Set instance
     */
    if ( !Info ) {
        Info = new Module();

        /**
         * Config module
         */
        Info.logic(Logic)
            .ui('ui', Ui)
            .ui('main.ui', MainUi)
            .ui('icons', Icons);

        Info.clone('data', toRaw(usePage().props.pageData.line));

        /**
         * If use filter set filter instance & filter config
         */
        Info.instance('filter', useFilter, ['info']);
        const Filter =  Info.instance('filter');
            Filter.set('fields', Info.get('data:filter.fields'))
                .set('data', Info.get('data:filter.data'));

            Filter.request(
                'limit',
                Filter.get('data:limit')
                    ? Filter.get('data:limit')
                    : 10
            );

            if ( Filter.get('data:sort') )
                Filter.request('sort', Filter.get('data:sort'));

            const page = Info.get('data:items') ? 2 : 1;
                Filter.page(page);
        /**/

        /**
         * If use dynamic data set controller instance & controller config
         */
        Info.instance('controller', useDataController, ['info']);
        const Controller = Info.instance('controller');

        Controller.set('data', Info.get('data:items'));
        /**/
    }

    /**
     * Return instance
     */
    return Info;
}
