import Main from '@/Modules/Widgets/Info/Logic/Main.vue';
import Head from '@/Modules/Widgets/Info/Logic/Head.vue';
import Body from '@/Modules/Widgets/Info/Logic/Body/Main.vue';
import BodyItem from '@/Modules/Widgets/Info/Logic/Body/Item.vue';

export default {
    'main': Main,
    'head': Head,
    'body': Body,
    'body.item': BodyItem,
}
