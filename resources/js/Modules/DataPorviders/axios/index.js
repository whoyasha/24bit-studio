import _ from 'lodash';
import axios from 'axios';
import { Module } from '@/Modules/Main/module.js';

export default class extends Module {

    #data = null;
    #url = null;
    loadingDelay = 500;

    constructor(controller, routeName, method) {
        super();
        this.Controller = controller;
        this.#url = route(routeName);
        this.method = method;
    }

    /**
     * Обёртка для форм
     */
    send = () => {
        this.#data = new FormData(this.Controller.ref('form').value);
        if ( this.method === 'get') {
            this.url();
        }
        const fn = this[this.method];
              fn.call(this,[]);
    }

    url = () => {
        const data = [...this.#data.entries()]
            .map(x => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
            .join('&');
        if (!this.#url.match(new RegExp(data)))
            this.#url = `${this.#url}?${data}`;
    }

    get = async () => {
        this.Controller.state('start-loading', true);
        this.Controller.state('loading', true);
        try {
            const response = await axios.get(this.#url)
            this.response(response);
        } catch (err) {
            this.Controller.state('error', err.code);
        }
    }

    post = async (data = false) => {
        data = data ? data : this.#data;
        this.Controller.state('start-loading', true);
        this.Controller.state('loading', true);
        try {
            const response = await axios.post(this.#url, data);
            this.response(response);
        } catch(err) {
            this.Controller.state('error', err.code);
        }
    }

    response = (response) => {
        this.Controller.state('result', response.data);
        setTimeout(() => {
            this.Controller.state('loading', false);
        }, this.loadingDelay);
    }
}

// axios.interceptors.response.use(
//   response => {
//       console.log('response in : ', response);
//     return response;
//   },
//   function(error) {
//     console.log('error in : ', error.response.data.message);
//     return Promise.reject(error.response);
//   }
// );
