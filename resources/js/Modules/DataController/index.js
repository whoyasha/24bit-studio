import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/DataController/Logic';

const dataInstance = {}

export function useDataController(id) {

    if ( !dataInstance[id] ) {

        const data = new DataController(id);

        /**
         * Config module
         */
        data.logic(Logic);
        data.states({
            isLoaded: false,
            isLoading: false,
            showNextBtn: false
        });

        dataInstance[id] = data;
    }

    return dataInstance[id];
}

class DataController extends Module {

    constructor (id) {
        super();
        this.id = id;
        return this;
    }

    /**
     * Current class logic
     */
    data = null;
    items = [];
    pages = [];

    getId = () => {
        return this.id;
    }

    load = () => {

    }

    toggleLoader = (show) => {
        this.state('isLoading').value = show;
        return this;
    }

    nextBtn = () => {
        const pageNum = this.get('data:pageNum');
        const pageCount = this.get('data:pageCount');
        this.state('showNextBtn', (pageNum > 0 && pageNum < pageCount));
    }

    nextPage = ( page = false ) => {

        if ( page === false && this.instance('filter') )
            return this.instance('filter').request('page');

        let nextPage = 0;
        const pageNum = this.get('data:pageNum')
            ? this.get('data:pageNum')
            : 0;

        if ( !isNaN(Number(page)) ) {
            nextPage = Number(page);
        }
        else if (page === 'next') {
            nextPage = Math.ceil(pageNum+1);
        }
        else if (page === 'prev' && pageNum > 1 ) {
            nextPage = Math.ceil(pageNum-1);
        }

        this.instance('filter').request('page', Number(nextPage));
        return this;
    }
}
