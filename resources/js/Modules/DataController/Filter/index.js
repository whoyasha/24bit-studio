import _ from 'lodash';
import { ref } from 'vue';
import { Module } from '@/Modules/Main/module.js';

const filterInstance = {}

export function useFilter(id) {

    if ( !filterInstance[id] ) {
        const filter = new FilterModule(id);

        /**
         * Config module
         */
        // filter.ui('ui', Ui);
        filter.states({
            isFiltered: false,
            isSorted: false
        });

        filterInstance[id] = filter;
    }

    return filterInstance[id];
}

class FilterModule extends Module {

    #url = null;
    #request = null;
    filter = ref({});

    constructor(id) {
        super();
        this.id = id;
        this.#url = new URL(route(`${this.id}.api`));
        this.#request = this.#url.searchParams
        return this;
    }

    url = () => {
        _.map(this.all(), (value, key) => {
            this.request(key, value);
        });
        return this.#url;
    }

    add = ( key, value ) => {
        if ( this.state('isFiltered').value )
            this.state('isFiltered', false);

        this.set(`filter:${key}`, value, true);
        this.state('isFiltered', true);
        return this;
    }

    delete = (key) => {
        delete this.filter.value[key];
        if ( !this.check() )
            this.state('isFiltered', false);

        return this;
    }

    check = () => {
        return Object.keys(this.filter.value).length > 0;
    }

    all = (value = false) => {
        if ( value ) {
            this.set('filter', value, true);
            this.state('isFiltered', true);
            return this;
        }
        return this.filter.value;
    }

    clear = () => {
        this.all({});
        this.state('isFiltered', false);
        return this;
    }

    request = (key, value) => {
        this.#request.append(key, value);
    }

    page = ( page = false ) => {

        if ( page === false )
            return this.request('page');

        let nextPage = 0;
        const pageNum = this.get('data:pageNum')
            ? this.get('data:pageNum')
            : 0;

        if ( !isNaN(Number(page)) ) {
            nextPage = Number(page);
        }
        else if (page === 'next') {
            nextPage = Math.ceil(pageNum+1);
        }
        else if (page === 'prev' && pageNum > 1 ) {
            nextPage = Math.ceil(pageNum-1);
        }

        this.request('page', Number(nextPage));
        return this;
    }
}
