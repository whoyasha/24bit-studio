import NavNext from '@/Modules/Slider/Ui/Nav/Next.vue';
import NavPrev from '@/Modules/Slider/Ui/Nav/Prev.vue';

export default {
    'nav.next': NavNext,
    'nav.prev': NavPrev,
}
