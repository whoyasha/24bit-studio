import {
    IconChevronLeft,
    IconChevronRight
 } from '@tabler/icons-vue';

export default {
    prev: IconChevronLeft,
    next: IconChevronRight,
}
