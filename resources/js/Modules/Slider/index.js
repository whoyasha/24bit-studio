import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Slider/Logic';
import Ui from '@/Modules/Slider/Ui';
import Icons from '@/Modules/Slider/Ui/icons.js';

let sliderInstance = {};

export function useSlider(id) {
    if ( !sliderInstance[id] ) {
        const slider = new MSlider();

        /**
         * Config module
         */
        slider.logic(Logic)
            .ui('ui', Ui)
            .ui('icons', Icons);

        slider.set('id', id);
        slider.refs(['nav-prev','nav-next']);
        sliderInstance[id] = slider;
    }
    return sliderInstance[id];
}

class MSlider extends Module {
    auto = () => {
        const nextBtn = this.ref('nav-next');
    }
}
