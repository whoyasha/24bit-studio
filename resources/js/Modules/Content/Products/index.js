import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Content/Products/Logic';
import Ui from '@/Modules/Content/Products/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import ArticlesUi from '@/Modules/Content/Articles/Ui';
import ArticlesLogic from '@/Modules/Content/Articles/Logic';


import { toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';

import { useDataController } from '@/Modules/DataController';

import { useCorporate } from '@/Modules/Widgets/Corporate';

export function useProducts(pageType) {

    const productsPage = new Module(pageType);

    /**
     * Config module
     */
    productsPage.logic(Logic)
        .import('articles.item', ArticlesLogic.item)
        .ui('ui', Ui)
        .ui('main.ui', MainUi)
        .ui('ui', ArticlesUi)
        .ui('icons', Icons);

        /**
         * TODO .ui('ui', ArticlesUi) продумать чтобы не пересекались view при проброске в другие модули Здесь вроде нет
         */


    productsPage.use('corporate', useCorporate);

    if ( pageType === 'main' ) {
        productsPage.clone('data', toRaw(usePage().props.pageData.products));
    }
    else {
        productsPage.clone('data', toRaw(usePage().props.pageData.detail));
    }

    productsPage.set('page:type', pageType);

    /**
     * Return instance
     */
    return productsPage;
}
