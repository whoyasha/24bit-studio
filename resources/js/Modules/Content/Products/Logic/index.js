import Module from '@/Modules/Main/Logic/Module.vue';
import Main from '@/Modules/Content/Products/Logic/Main.vue';
import Detail from '@/Modules/Content/Products/Logic/Detail.vue';
import Services from '@/Modules/Content/Products/Logic/Services.vue';
import DetailCorporate from '@/Modules/Content/Products/Logic/DetailCorporate.vue';
export default {
    'main': Main,
    'detail': Detail,
    'module': Module,
    'services': Services,
    'detail-corporate': DetailCorporate
}
