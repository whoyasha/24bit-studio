import Module from '@/Modules/Main/Logic/Module.vue';
import Main from '@/Modules/Content/Support/Logic/Main.vue';
import Contacts from '@/Modules/Content/Support/Logic/Contacts.vue';
import Lk from '@/Modules/Content/Support/Logic/Lk.vue';
export default {
    'main': Main,
    'contacts': Contacts,
    'lk': Lk
}
