import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Content/About/Logic';
import Ui from '@/Modules/Content/About/Ui';
import MainUi from '@/Modules/Main/Ui';
import ContactsUi from '@/Modules/Content/Contacts/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import { toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';

export function useAboutPage() {

    const aboutPage = new Module();

    /**
     * Config module
     */
    aboutPage.logic(Logic)
        .ui('ui', Ui)
        .ui('main.ui', MainUi)
        .ui('contacts.ui', ContactsUi)
        .ui('icons', Icons)
        .useLayouts();

        aboutPage.set('data', toRaw(usePage().props.pageData.content));

    /**
     * Return instance
     */
    return aboutPage;
}
