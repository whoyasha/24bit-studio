import Module from '@/Modules/Main/Logic/Module.vue';
import Main from '@/Modules/Content/Contacts/Logic/Main.vue';
import Requisites from '@/Modules/Content/Contacts/Logic/Requisites.vue';
export default {
    'main': Main,
    'module': Module,
    'requisites': Requisites
}
