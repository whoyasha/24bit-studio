import Main from '@/Modules/Content/Contacts/Ui/Main.vue';
import Phones from '@/Modules/Content/Contacts/Ui/Phones.vue';
import Phone from '@/Modules/Content/Contacts/Ui/Phone.vue';
import Emails from '@/Modules/Content/Contacts/Ui/Emails.vue';
import Email from '@/Modules/Content/Contacts/Ui/Email.vue';
import Requisites from '@/Modules/Content/Contacts/Ui/Requisites.vue';
import Requisite from '@/Modules/Content/Contacts/Ui/Requisite.vue';
import Buttons from '@/Modules/Content/Contacts/Ui/Buttons.vue';

export default {
    'main': Main,
    'phones': Phones,
    'phone': Phone,
    'emails': Emails,
    'email': Email,
    'requisites': Requisites,
    'requisite': Requisite,
    'buttons': Buttons
}
