import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Content/Contacts/Logic';
import Ui from '@/Modules/Content/Contacts/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import { useYandexMap } from '@/Modules/YandexMap';

import { toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';

export function useContactsPage() {
    /**
     * Set instance
     */

    const contactPage = new Module();

    /**
     * Config module
     */
    contactPage.logic(Logic)
        .ui('ui', Ui)
        .ui('main.ui', MainUi)
        .ui('icons', Icons)
        .useLayouts();

    contactPage.use('map', useYandexMap);

    contactPage.set('data', usePage().props.pageData.contacts);
    contactPage.set('data:detail', usePage().props.pageData.detail);

    /**
     * Return instance
     */
    return contactPage;
}
