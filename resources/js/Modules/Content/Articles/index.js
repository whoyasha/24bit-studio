import _ from 'lodash';
import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Content/Articles/Logic';
import Ui from '@/Modules/Content/Articles/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import { useDataController } from '@/Modules/DataController';
import { useFilter } from '@/Modules/DataController/Filter';

import { toRaw } from 'vue';
import { usePage } from '@inertiajs/vue3';

export function useArticles(options) {
    /**
     * Set instance
     */
    const Articles = new Module();

    /**
     * Config module
     */
    Articles.logic(Logic)
        .ui('ui', Ui)
        .ui('main.ui', MainUi)
        .ui('icons', Icons);

    let data = null;
    if ( route().current() === 'page.articles' ) {
        const rawData = toRaw(usePage().props.pageData);
        data = {
            filter: {categories: rawData.content_categories},
            top: rawData.topArticle,
            items: rawData.articleItems,
            params: rawData.articleParams
        }
    }
    else {
        data = toRaw(usePage().props.pageData.articles)
    }

    Articles.set('options', options);

    /**
     * If you want to use filter, set filter instance & filter config
     */
    if ( options.setFilter ) {
        Articles.instance('filter', useFilter, ['articles']);
        const Filter =  Articles.instance('filter');
            Filter.set('fields', data.filter.fields)
                .set('data', data.filter.data);

            Filter.request(
                'limit',
                Filter.get('data:limit')
                    ? Filter.get('data:limit')
                    : 10
            );

            if ( Filter.get('data:sort') )
                Filter.request('sort', Filter.get('data:sort'));

            const page = data.items ? 2 : 1;
                Filter.page(page);
    }
    /**/

    /**
     * If you want to use dynamic data, set controller instance & controller config
     */
        Articles.instance('controller', useDataController, ['articles']);
        const Controller = Articles.instance('controller');

            /**
             * If isset data via page load
             */
            Controller.set('data', data);

            /**
             * else
             */
                //get data via api request ( an appropriate handler is required )
    /**/

    /**
     * Return instance
     */
    return Articles;
}
