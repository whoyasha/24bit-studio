import Main from '@/Modules/Content/Articles/Ui/Main.vue';

import Head from '@/Modules/Content/Articles/Ui/Head/Main.vue';

import Body from '@/Modules/Content/Articles/Ui/Body/Main.vue';

import ItemTop from '@/Modules/Content/Articles/Ui/Item/Top.vue';
import ItemTopInner from '@/Modules/Content/Articles/Ui/Item/TopInner.vue';
import ItemsList from '@/Modules/Content/Articles/Ui/Item/List.vue';
import ItemBodyWrapper from '@/Modules/Content/Articles/Ui/Item/BodyWrapper.vue';
import ItemBodyInner from '@/Modules/Content/Articles/Ui/Item/BodyInner.vue';

import Preloader from '@/Modules/Content/Articles/Ui/Preloader.vue';
import Foot from '@/Modules/Content/Articles/Ui/Foot.vue';

export default {
    'main': Main,

    'head': Head,
    'body': Body,

    'item.top': ItemTop,
    'item.top-inner': ItemTopInner,
    'items.list': ItemsList,
    'item.body-wrapper': ItemBodyWrapper,
    'item.body-inner': ItemBodyInner,

    'preloader': Preloader,
    'foot': Foot
}
