import Main from '@/Modules/Content/Articles/Logic/Main.vue';
import Head from '@/Modules/Content/Articles/Logic/Head.vue';
import Body from '@/Modules/Content/Articles/Logic/Body.vue';
import Item from '@/Modules/Content/Articles/Logic/Item.vue';
import Foot from '@/Modules/Content/Articles/Logic/Foot.vue';

export default {
    'main': Main,
    'head': Head,
    'body': Body,
    'item': Item,
    'foot': Foot,
}
