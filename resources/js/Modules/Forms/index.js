import _ from 'lodash';
import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Forms/Logic';
import Ui from '@/Modules/Forms/Ui';
import Icons from '@/Modules/Forms/Ui/icons.js';
import MainIcons from '@/Modules/Main/Ui/icons.js';
import MainUi from '@/Modules/Main/Ui';

import { ref, watchEffect } from 'vue'
import { useFieldsView, useFieldsController } from '@/Modules/Forms/Fields';

import Notyfy from '@/Modules/Notify';

export function useFormController(name, fields, checker = false) {

    const form = new FormController(name);

    /**
     * Config module
     */
    form.logic(Logic)
        .ui('ui', Ui)
        .ui('main.ui', MainUi)
        .ui('icons', Icons)
        .ui('icons', MainIcons);

    form.use('fieldView', useFieldsView);
    form.bind('fieldController', useFieldsController, [form, checker]);

    fields = _.reject(fields, (field) => {
        return _.isUndefined(field.form_type);
    });
    form.set('data:fields', fields);
    form.use('notify', Notyfy);

    watchEffect(() => {
        if (form.state('error').value)
            form.error();
    });

    return form;
}

class FormController extends Module {

    #states = {}

    constructor(name) {
        super();

        this.set('name', name);
        this.refs(['form', 'success', 'error']);

        this.state('start-loading', false);
        this.state('result', false);
        this.state('loading', false);
        this.state('error', false);

        /**
         * TODO сделать показ формы по загрузке всех кастомных чекеров и полей
         * возможно под каждый загружаемый элемент
         *
         this.ready();
        /**/
        this.ready(true);

        return this;
    }

    submit = (method = false) => {
        let errors = false;

        if ( !_.isUndefined(this.get('checkers')) )
            errors = this.check();

        if ( errors )
            return false;

        const provider = this.instance('data-provider');

        if ( method )
            provider.set('method', method);

        provider.send();
    }

    error = () => {
        const provider = this.get('callback:provider.error', 'notify');
        if ( provider === 'notify' ) {
            const error = this.state('error').value;
            this.notify(this.get('callback:mess.error', `Ошибка отправки: ${error}. Попробуйте позже.`), 'error');
        }
    }

    success = (mess) => {
        const provider = this.get('callback:provider.error', 'notify');
        if ( provider === 'notify' ) {
            this.notify(this.get('callback:mess.error', mess), 'success');
        }
    }

    check = () => {
        _.map(this.get('checkers'), (checker, key) => {
            const check = checker.check();
            checker.state('error', check);
            this.set(`errors:${key}`, check);
        });
        return _.includes(this.get('errors'), true);
    }

    clear = () => {
        _.map(this.get('fieldControllers'), (controller, key) => {
            controller.clear();
        });
    }
}
