import Wrapper from '@/Modules/Forms/Ui/Wrapper.vue';
import Footer from '@/Modules/Forms/Ui/Footer.vue';

export default {
    'wrapper': Wrapper,
    'footer': Footer
}
