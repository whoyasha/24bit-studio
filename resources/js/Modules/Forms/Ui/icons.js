import {
    IconChevronLeft,
    IconChevronRight,
    IconUserCheck,
    IconX,
    IconUserCircle,
    IconBuildingBank,
    IconPhone,
    IconAt
 } from '@tabler/icons-vue';

export default {
    prev: IconChevronLeft,
    next: IconChevronRight,
    userconcent: IconUserCheck,
    close: IconX,
    user: IconUserCircle,
    company: IconBuildingBank,
    phone: IconPhone,
    email: IconAt
}
