import _ from 'lodash';
import { ref } from 'vue';
import { Module } from '@/Modules/Main/module.js';

export default function(field) {
    return new FormChecker(field);
}



class FormChecker extends Module {

    #states = {}

    #patterns = {
        tel: /(\d{10})|(^8|7|\+7)((\d{10})|(\s\(\d{3}\)\s\d{3}\s\d{2}\s\d{2})|(\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}))/,
        telstrict: /(\d{10})|(^\+7)(\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2})/,
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([а-яёА_ЯЁa-zA-Z\-0-9]+\.)+[а-яёА_Яa-zA-Z]{2,}))$/,
        // tnved: /\d{4,10}/
        // numeric: /^[0-9]+$/
    }

    constructor(field) {
        super();

        this.set('field', field);
        this.set('checkLength', !_.isUndefined(field.validate?.length));
        this.state('error', false);

        return this;
    }

    test = (type, value) => {
        return !this.#patterns[type].test(value);
    }

    pattern = (type) => {
        return this.#patterns[type];
    }

    checker = () => {
        const type = this.get('type');
        return _.isFunction(this[type])
            ? this[type]
            : null;
    }

    check = () => {
        return this.get('checker')
            .apply(this, []);
    }

    length = () => {
        return _.toString(this.get('value')).length;
    }

    /**
     * Если validate - объект, проверяем по max&min
     * Если boolean - проверяем больше 1го
     */
    #length = () => {
        const value = this.get('value');
        const validate = this.get('field:validate.length');
        const length = this.length();

        if ( _.isNumber(validate) )
            return length !== validate;

        if ( validate.min && validate.max ) {
            return (
                length < validate.min ||
                length > validate.max
            );
        }
        else {
            return validate.min
                ? length < validate.min
                : length > validate.max;
        }
        return false;
    }

    text = () => {
        const value = this.get('value');

        if ( this.get('checkLength') )
            return this.#length();

        return false;
    }

    textarea = () => {
        return this.text();
    }

    tnved = () => {

        if ( this.get('checkLength') )
            return this.#length();

        const value = this.get('value');

        const pattern = /\d{4,10}/;
        return !pattern.test(value);
        // return this.test('tnved', value);
    }

    number = () => {
        if ( this.get('checkLength') )
            return this.length();
    }

    tel = () => {
        const value = this.get('value');

        if ( this.get('checkLength') && this.#length() )
            return true;

        return this.test('tel', value);
    }

    email = () => {
        const value = this.get('value');
        return this.test('email', value);
    }

    mess = {
        tnved: (mess) => {
            if ( !_.isUndefined(mess) )
                return mess;

            return 'от 4-х до 10-ти цифр'

            // const validate = this.get('field:validate.length');
            // if ( !_.isUndefined(validate) )
            //     return this.mess.length(validate, 'цифр');

            return false;
        },
        text: (mess) => {
            if ( !_.isUndefined(mess) )
                return mess;

            const validate = this.get('field:validate.length');
            if ( !_.isUndefined(validate) )
                return this.mess.length(validate, 'символов');

            return 'обязательно для заполнения'
        },
        tel: (mess) => {
            if ( !_.isUndefined(mess) )
                return mess;

            return 'не корректный телефон';
            return '+7 (ХХХ) ХХХ-ХХ-ХХ';
        },
        email: (mess) => {
            if ( !_.isUndefined(mess) )
                return mess;

            return 'не корректный email';
        },
        length: (validate, suff) => {
            if ( _.isNumber(validate) )
                return `ровно ${validate} ${suff}` ;

            if ( validate.min && validate.max ) {
                return `мин. ${validate.min}, макс. ${validate.max} ${suff}`;
            }
            else {
                return validate.min
                    ? validate.min === 1 ? 'Обязательно для заполнения' : `мин. ${validate.min} ${suff}`
                    : `макс. ${validate.max} ${suff}`
            }
            return false;
        }
    }
}


/**
 * TODO выяснить необходимость и принять решение об использовании
 */
// import = (checker) => {
//     import(`./types/${checker}.js`)
//         .then((obj) => {
//             this.use('checker', obj.default);
//         })
//         .catch((err) => {
//             import(`../custom/${checker}.js`)
//                 .then((obj) => {
//                     this.use('checker', obj.default);
//                 })
//                 .catch((err) => {
//                     console.warn(`FormChechers not found : `, err);
//                 });
//         });
// }
