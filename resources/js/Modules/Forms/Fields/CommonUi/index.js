import Wrapper from './Wrapper.vue';
import IconWrapper from './IconWrapper.vue';

export default {
    'wrapper': Wrapper,
    'icon-wrapper': IconWrapper
}
