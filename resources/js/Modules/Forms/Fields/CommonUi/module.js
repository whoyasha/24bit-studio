export class CommonUi {
    #ui = {}
    constructor() {
        return this;
    }

    ui = (name, data) => {
        this.#ui = ui;

        console.log('this.#ui in : ', this.#ui);
        return this;
    }

    view = (key) => {

        console.log('key in : ', key);
        const keys = key.split(':');
        const values = this.#ui[keys[0]];

        if ( keys.length < 2 )
            return null;

        // if ( keys.length === 2 && values )
            return values[keys[1]];
    }
}
