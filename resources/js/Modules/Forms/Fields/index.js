import _ from 'lodash';
import { Module } from '@/Modules/Main/module.js';
import { ref, defineAsyncComponent, watchEffect } from 'vue';
// import String from './views/String/Index.vue';
import Fields from './views';

import CommonUi from './CommonUi';

export function useFieldsViewDynamic(type) {
    // const typeName = _.capitalize(_.camelCase(type.split(':')[0]));
    // return defineAsyncComponent({
    //     loader: () => import(`./views/${typeName}/Index.vue`),
    //     delay: 200,
    //     timeout: 3000,
    //     errorComponent: String
    // });
}

export function useFieldsView(type) {
    const typeName = type.split(':')[0];
    return Fields[typeName];
}

export function useFieldsController(formController, checker) {
    const controller = new FieldController(formController);

    if ( checker ) {
        import(`./checkers/${checker}/index.js`)
            .then((obj) => {
                controller.use('checker', obj.default);
                controller.ready(true);
            })
            .catch((err) => {
                console.warn(`FormChechers error : `, err);
                controller.ready(false);
            });
    }
    else {
        controller.ready(true);
    }

    controller.ui('common.ui', CommonUi);

    return controller;
}

class FieldController extends Module {

    #states = {}
    #value = ref(null);

    constructor(formController) {
        super();
        this.FormController = formController;
        this.Checker = null;
        return this;
    }

    type = () => {
        const type = this.get('field:form_type');
        const parse = type.split(':');
        return parse.length === 2
                ? parse[1]
                : type;
    }

    init = () => {

        if ( _.isUndefined(this.get('field:label'))
                && !_.isUndefined(this.get('field:title')) ) {
            this.set('field:label', this.get('field:title'));
        }

        this.value(
            _.isUndefined(this.get('field:value'))
                ? null
                : this.get('field:value')
        );
        this.state('isEmpty', null === this.value().value);

        if ( this.get('field:required') === true
                && !this.get('field:validate.length') ) {
            this.set('field:validate.length', {min: 1});
        }

        this.set('needChecker', !_.isUndefined(this.get('field:validate')));
        this.state('isFocus', false);

        const code = this.get('field:code');
        this.FormController.set(`fieldControllers:${code}`, this);

        this.state(
            'focus',
            _.isUndefined(this.get('field:autofocus'))
                ? false
                : this.get('field:autofocus')
        );

        watchEffect(() => {
            if ( this.ready()
                    && this.bind('checker')
                        && this.get('needChecker')
                            && !this.Checker )  {
                /**
                 * Инициализируем чекер
                 */
                this.Checker = this.checker(this.get('field'));
                this.Checker.set('value', this.value());
                this.Checker.set('type', this.type());
                this.Checker.set('checker', this.Checker.checker());

                /**
                 * Связываем чекер с FormController для проверки полей при submit
                 */
                const code = this.get('field:code');
                this.FormController.set(`checkers:${code}`, this.Checker);
                /**/
            }
        });
    }

    value = (value = undefined) => {
        if ( !_.isUndefined(value) )
            this.#value.value = value;

        return this.#value;
    }

    check = () => {
        if ( !this.Checker )
            return true;

        this.Checker.state('error', this.Checker.check());
        this.state('isEmpty', this.Checker.length() === 0);
    }

    clear = () => {
        this.value(null);
        this.state('isEmpty', true);

        if ( this.Checker )
            this.Checker.state('error', false);
    }
}
