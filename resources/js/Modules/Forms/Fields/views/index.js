import Checkbox from '@/Modules/Forms/Fields/views/Checkbox/Index.vue';
import Hidden from '@/Modules/Forms/Fields/views/Hidden/Index.vue';
import Select from '@/Modules/Forms/Fields/views/Select/Index.vue';
import String from '@/Modules/Forms/Fields/views/String/Index.vue';
import Tnved from '@/Modules/Forms/Fields/views/Tnved/Index.vue';

export default {
    checkbox: Checkbox,
    hidden: Hidden,
    select: Select,
    string: String,
    tnved: Tnved
}
