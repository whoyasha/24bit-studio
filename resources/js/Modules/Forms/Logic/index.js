import Main from '@/Modules/Forms/Logic/Main.vue';
import Userconsent from '@/Modules/Forms/Logic/Userconsent.vue';

export default {
    main: Main,
    userconsent: Userconsent
}
