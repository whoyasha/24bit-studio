import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/PageData/Logic';
import Ui from '@/Modules/PageData/Ui';
import MainUi from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons';

import { ref, toRaw } from 'vue';
import { usePage, router } from '@inertiajs/vue3';

export function usePageData() {

    const pageInstance = new Module();

    /**
     * Config module
     */
    pageInstance.logic(Logic)
         .ui('ui', Ui)
         .ui('main.ui', MainUi)
         .ui('icons', Icons);

    pageInstance.set('data', toRaw(usePage().props));

    const currentSection = router.page.url.split('/')[1];
    pageInstance.set('currentSection', `\/${currentSection}`);
    pageInstance.set('currentPage', router.page.url);

    const showPageTitle = (
        pageInstance.get('data:pageData.showPageTitle') &&
        pageInstance.get('data:pageData.pagetitle')
    )
    pageInstance.set('showPageTitle', showPageTitle);

    const showBreadcrumbs = (
        pageInstance.get('data:pageData.showPageTitle') &&
        pageInstance.get('data:pageData.breadcrumbs')
    )
    pageInstance.set('showBreadcrumbs', showBreadcrumbs);

    return pageInstance;
}
