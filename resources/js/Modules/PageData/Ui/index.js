import PageTitle from '@/Modules/PageData/Ui/PageTitle.vue';
import Breadcrumbs from '@/Modules/PageData/Ui/Breadcrumbs/Main.vue';
import BreadcrumbsItem from '@/Modules/PageData/Ui/Breadcrumbs/Item.vue';

/**
 * Ключи базовых компонентов (без точки || Main.vue || Wrapper.vue) должны соответствовать ключам pageData
 */
export default {
    'pagetitle': PageTitle,
    'breadcrumbs': Breadcrumbs,
    'breadcrumbs.item': BreadcrumbsItem
}
