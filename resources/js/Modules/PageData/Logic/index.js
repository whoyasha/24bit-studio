import PageTitle from '@/Modules/PageData/Logic/PageTitle.vue';
import Breadcrumbs from '@/Modules/PageData/Logic/Breadcrumbs.vue';

export default {
    pagetitle: PageTitle,
    breadcrumbs: Breadcrumbs
}
