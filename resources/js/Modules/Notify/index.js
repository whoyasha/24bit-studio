import { useToast } from 'vue-toast-notification';
import('@/Modules/Notify/theme.css');

export default (mess, type = 'info', duration = 5000, position = 'top-right') => {
    const toaster = useToast();
            toaster.open({
                message: mess,
                type: type,
                pauseOnHover: true,
                duration: duration,
                position: position
            });
}
