import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/YandexMap/Logic';
import Ui from '@/Modules/YandexMap/Ui';

import { onMounted, watchEffect } from 'vue';

export function useYandexMap() {

    const mapInstance = new YandexMap();

    /**
     * Config module
     */
    mapInstance.logic(Logic);
    mapInstance.ui('ui', Ui);

    mapInstance.state('isLoadApi', false);

    watchEffect(() => {
        mapInstance.initMap();
    });

    return mapInstance;
}

class YandexMap extends Module {

    constructor() {
        super();
        this.map = null;
        this.state('isInit', false);
        return this;
    }

    init = async () => {
        this.api = document.getElementById('yamap');

        if ( this.api ) {
            this.state('isLoadApi', true);
            return this;
        }

        this.api = document.createElement('script');
        this.api.async = true;
        this.api.setAttribute('src', 'https://api-maps.yandex.ru/v3/?apikey=b2368406-867f-4010-a7cb-22b55d184caa&lang=ru_RU');
        this.api.setAttribute('id', 'yamap');
        document.head.appendChild(this.api);

        this.api.addEventListener('load', () => {
            this.state('isLoadApi', true);
        });

        return this;
    }

    initMap = async () => {

        if ( !this.state('isLoadApi').value )
            return false;

        if ( this.state('isInit').value )
            return false;

        this.ymaps3 = ymaps3;
        await this.ymaps3.ready;

        const { YMap, YMapDefaultSchemeLayer } = this.ymaps3;
        this.map = new YMap(
            this.ref('container').value,
            {
                location: {
                    center: this.get('options:center'),
                    zoom: this.get('options:zoom')
                }
            }
        );
        this.map.addChild(new YMapDefaultSchemeLayer());

        /**
         * Behaviors
         */
        this.map.setBehaviors(this.get('options:behaviors'));

        /**
         * Zoom Controls
         */
        const { YMapControls } = this.ymaps3;
        const { YMapZoomControl } = await this.ymaps3.import('@yandex/ymaps3-controls@0.0.1');
        this.map.addChild(
          new YMapControls({position: 'right'})
            .addChild(new YMapZoomControl({}))
        );

        this.state('isInit', true);

        /**
         * Markers
         *
         * For custom markers ->  const { YMapMarker } = ymaps3;
         */

        const { YMapDefaultFeaturesLayer } = this.ymaps3;
        const { YMapDefaultMarker } = await this.ymaps3.import('@yandex/ymaps3-markers@0.0.1');
        this.map.addChild(new YMapDefaultFeaturesLayer());

        const markers = this.get('options:markers');
        markers.forEach((marker) => {
            this.map.addChild(
                new YMapDefaultMarker({
                    coordinates: marker.coords,
                    color: this.get('options:marker-color'),
                    popup: {
                        content: marker.popup.content,
                        position: this.get('options:popup-position')
                    }
                })
            );
        });

        // this.api.removeEventListener('load');

        return this;
    }
}
