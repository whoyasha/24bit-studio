import _ from 'lodash';
import { Module } from '@/Modules/Main/module.js';
import Logic from '@/Modules/Main/Logic';
import Ui from '@/Modules/Main/Ui';
import Icons from '@/Modules/Main/Ui/icons.js';

import { useScrollTo } from '@/Modules/Main/Ui/useScrollTo.js';

let mainInstance = null;

export function useMain() {
    if ( !mainInstance ) {
        mainInstance = new MainModule();

        /**
         * Config module
         */
        mainInstance.logic(Logic);
        mainInstance.ui('ui', Ui);
        mainInstance.ui('icons', Icons);
        mainInstance.states({
            contactsIsActive: false,
            infoIsActive: false,
            servicesIsActive: false,
            menuIsActive: false,
            asideIsActive: false
        });

        mainInstance.use('scrollTo', useScrollTo);
    }

    return mainInstance;
}

class MainModule extends Module {

    constructor () {
        super();
        return this;
    }

    setMobileActive = (item) => {
        const _this = this;
        _.map(this.states(), function(value, key) {
            if ( key !== item )
                _this.state(key, false);
        });

        this.state(item, !this.state(item).value);

        if ( item !== 'menuIsActive' && this.state(item).value )
            this.state('asideIsActive', true);
        else
            this.state('asideIsActive', false);
    }

    closeAside = () => {
        this.state('asideIsActive', false);
        this.state('menuIsActive', false);
    }
}
