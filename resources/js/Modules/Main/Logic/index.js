import Module from '@/Modules/Main/Logic/Module.vue';
import HeaderNav from '@/Modules/Main/Logic/HeaderNav.vue';
import PageHead from '@/Modules/Main/Logic/PageHead.vue';
import Footer from '@/Modules/Main/Logic/Footer.vue';

export default {
    'module': Module,
    'header.nav': HeaderNav,
    'page-head': PageHead,
    'footer': Footer
}
