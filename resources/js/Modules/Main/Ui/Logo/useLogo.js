import { computed } from 'vue';

export function useLogo(props) {
    const themes = {
        color: {
            st0: '#FF8314',
            st1: '#606466',
            st2: '#FFFFFF'
        },
        grayscale: {
            st0: '#FF8314',
            st1: '#606466',
            st2: '#FFFFFF'
        },
        gray: {
            st0: '#a4acb0',
            st1: '#606466',
            st2: '#FFFFFF'
        }
    }

    const fill = themes[props.theme];
    const filter = props.theme === 'grayscale'
        ? 'grayscale'
        : '';

    const sizes = {
        sm: 'h-6',
        md: 'h-10 lg:h-10 xl:h-12 2xl:h-10',
        lg: 'h-24'
    }

    const size = sizes[props.size];
    const isHover = computed(() => {return props.logoIsHover});

    return { fill, size, isHover, filter }
}
