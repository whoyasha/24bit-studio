import LogoDefault from '@/Modules/Main/Ui/Logo/Default.vue';
import LogoNewYear from '@/Modules/Main/Ui/Logo/NewYear.vue';
import Link from '@/Modules/Main/Ui/Base/Link.vue';
import InlineLink from '@/Modules/Main/Ui/Base/InlineLink.vue';
import ButtonLink from '@/Modules/Main/Ui/Base/ButtonLink.vue';
import Modal from '@/Modules/Main/Ui/Base/Modal.vue';
import Button from '@/Modules/Main/Ui/Base/Button.vue';
import Loader from '@/Modules/Main/Ui/Base/Loader.vue';
import Overlay from '@/Modules/Main/Ui/Base/Overlay.vue';
import Copy from '@/Modules/Main/Ui/Base/Copy.vue';
import Download from '@/Modules/Main/Ui/Base/Download.vue';

import MainDivider from '@/Modules/Main/Ui/Main/Divider.vue';

import AsideWrapper from '@/Modules/Main/Ui/Aside/Wrapper.vue';
import AsideItem from '@/Modules/Main/Ui/Aside/Item.vue';

import HeaderWrapper from '@/Modules/Main/Ui/Header/Wrapper.vue';
import HeaderMenu from '@/Modules/Main/Ui/Header/Menu/Menu.vue';
import HeaderMenuItem from '@/Modules/Main/Ui/Header/Menu/Item.vue';
import HeaderMenuDivider from '@/Modules/Main/Ui/Header/Menu/Divider.vue';
import HeaderMenuDividerVertical from '@/Modules/Main/Ui/Header/Menu/DividerVertical.vue';

import HeaderMobileButtons from '@/Modules/Main/Ui/Header/MobileButtons/Wrapper.vue';
import HeaderMobileButtonsItem from '@/Modules/Main/Ui/Header/MobileButtons/Item.vue';
import HeaderMobileButtonButton from '@/Modules/Main/Ui/Header/MobileButtons/Button.vue';

import ContentCard from '@/Modules/Main/Ui/Content/Card.vue';
import ContentBlockTitle from '@/Modules/Main/Ui/Content/BlockTitle.vue';
import ContentBlockDivider from '@/Modules/Main/Ui/Content/BlockDivider.vue';

import ContentTeaser from '@/Modules/Main/Ui/Content/Teaser.vue';

import WidgetsTitle from '@/Modules/Main/Ui/Widgets/Title.vue';
import WidgetsDescription from '@/Modules/Main/Ui/Widgets/Description.vue';

import PolicyPers from '@/Modules/Main/Ui/Policy/Pers.vue';
import PolicyCookies from '@/Modules/Main/Ui/Policy/Cookies.vue';

export default {
    'main.divider': MainDivider,

    'link': Link,
    'inline-link': InlineLink,
    'button-link': ButtonLink,
    'modal': Modal,
    'button': Button,
    'loader': Loader,
    'overlay': Overlay,
    'copy': Copy,
    'download': Download,

    'aside.wrapper': AsideWrapper,
    'aside.item': AsideItem,

    'logo.default': LogoDefault,
    'logo.ny': LogoDefault,

    'header.wrapper': HeaderWrapper,

    'header.menu': HeaderMenu,
    'header.menu.item': HeaderMenuItem,
    'header.menu.divider': HeaderMenuDivider,
    'header.menu.divider-vertical': HeaderMenuDividerVertical,

    'header.menu.mobile-buttons': HeaderMobileButtons,
    'header.menu.mobile-buttons.item': HeaderMobileButtonsItem,
    'header.menu.mobile-buttons.button': HeaderMobileButtonButton,

    'content.card': ContentCard,
    'content.block-title': ContentBlockTitle,
    'content.block-divider': ContentBlockDivider,
    'content.teaser': ContentTeaser,

    'widget.title': WidgetsTitle,
    'widget.description': WidgetsDescription,
    'policy_pers': PolicyPers,
    'policy_cookies': PolicyCookies,
}
