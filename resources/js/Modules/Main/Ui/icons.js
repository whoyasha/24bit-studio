import {
    IconApps,
    IconShoppingCart,
    IconHeadset,
    IconBuildingSkyscraper,
    IconAddressBook,
    IconMenu2,
    IconX,
    IconInfoSquareRounded,
    IconDeviceImacStar,
    IconPhone,
    IconCalendarEvent,
    IconExternalLink,
    IconChevronDown,
    IconChevronRight,
    IconDots,
    IconDotsVertical,
    IconQrcode,
    IconHome,
    IconArrowNarrowRight,
    IconCircleArrowUpFilled,
    IconMapPin,
    IconNews,
    IconCircleCheckFilled,
    IconCircleCheck,IconCircleOff,
    IconClock,
    IconAt,
    IconChecklist,
    IconMessageDollar,
    IconRotateRectangle,
    IconTransform,

    IconInfoCircle,
    IconTerminal2,
    IconLicense,
    IconDevicesCog,
    IconUserScreen,

    IconBubbleText,
    IconMessageBolt,
    IconDeviceDesktopAnalytics,
    IconMailStar

} from '@tabler/icons-vue';

export default {
    products: IconDeviceImacStar,
    support: IconHeadset,
    about: IconBuildingSkyscraper,
    contacts: IconAddressBook,
    menu: IconMenu2,
    close: IconX,
    info: IconInfoSquareRounded,
    services: IconApps,
    phone: IconPhone,
    calendar: IconCalendarEvent,
    home: IconHome,
    qr: IconQrcode,
    map: IconMapPin,
    news: IconNews,
    clock: IconClock,
    email: IconAt,
    order: IconMessageDollar,
    edo: IconTransform,

    consulting: IconInfoCircle,
    dev: IconTerminal2,
    license: IconLicense,
    service: IconDevicesCog,
    lk: IconUserScreen,
    knowelege: IconBubbleText,
    'consalt-mess': IconMessageBolt,
    'consult-service': IconDeviceDesktopAnalytics,
    'mail-star': IconMailStar,

    'check-list': IconChecklist,
    'ext-link': IconExternalLink,
    'chevron-down': IconChevronDown,
    'chevron-right': IconChevronRight,
    'h-dots': IconDots,
    'v-dots':IconDotsVertical,
    'arrow-right-sm': IconArrowNarrowRight,
    'check-cf': IconCircleCheckFilled,
    'check-c': IconCircleCheck,
    'off-c': IconCircleOff,
    'top-link': IconCircleArrowUpFilled
}
