const borders = {
    transparent: 'border-transparent',
    white: 'border-white',
    primary: 'border-primary',
    info: 'border-blue-400',
    warning: 'border-orange-400',
    danger: 'border-red-400',
    success: 'border-green-400',
    default: 'border-neutral-300',

    violet: 'border-violet-400',
    'violet-dark': 'border-violet-700',
    'amber-dark': 'border-amber-800',
}

const shadows = {
    visible: 'shadow-lg shadow-gray-300',
    hover: 'hover:shadow-lg shadow-gray-300',
    default: ''
}

const backgrounds = {
    white: 'bg-white',
    primary: 'bg-primary',
    info: 'bg-blue-500',
    warning: 'bg-orange-600',
    danger: 'bg-red-600',
    success: 'bg-green-600',
    default: 'bg-neutral-600',
    'success-light': 'bg-green-100',
    'default-light': 'bg-neutral-200',

    lime: 'bg-lime-600',
    amber: 'bg-amber-600',
    emerald: 'bg-emerald-500',
    fuchsia: 'bg-fuchsia-400',
    indigo: 'bg-indigo-400',
    violet: 'bg-violet-400',
    'violet-light': 'bg-violet-200',
    yellow: 'bg-yellow-500',
}

const rings = {
    primary: 'focus:ring-primary',
    info: 'focus:ring-blue-400',
    warning: 'focus:ring-orange-400',
    danger: 'focus:ring-red-400',
    success: 'focus:ring-green-400',
    default: 'focus:ring-neutral-300',

    violet: 'focus:ring-violet-400',
    'violet-dark': 'focus:ring-violet-700',
    'amber-dark': 'focus:ring-amber-800',
}

export { borders, backgrounds, rings, shadows }
