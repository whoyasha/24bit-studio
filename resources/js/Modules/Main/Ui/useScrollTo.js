export function useScrollTo(event) {
    if ( typeof event === 'object' )
        event.preventDefault();

    const targetId = typeof event === 'string'
        ? ( event.match(new RegExp('(\#)'))
            ? event.replace('#', '')
            : event )
        : event.target?.href.replace('#', '');

    const target = document.getElementById(targetId);

    if ( !target )
        return false;

    target.scrollIntoView(
        {behavior: "smooth", block: "center", inline: "start"}
    );
}
