import _ from 'lodash';
import { ref, isRef, toRef, toRaw, reactive } from 'vue';
import Dot from '@/Modules/Utils/dot.js';

import DotDev from '@/Modules/Utils/dot_dev.js';

export class Module {

    #ready = ref(false);
    #states = {}
    #reactives = {}
    #refs = {}
    #utils = {}
    #logic = {}
    #ui = {}



    constructor () {
        this.utils('dot', Dot);
        this.layouts = ref(null);
        return this;
    }

    ready = (value = undefined) => {
        if ( undefined !== value )
            this.#ready.value = value;

        return this.#ready.value;
    }

    ref = (key) => {
        if ( !this.#refs[key] )
            this.#refs[key] = ref(null);

        return this.#refs[key];
    }

    refs = (arr) => {
        arr.forEach((item) => {
            this.ref(item);
        })
    }

    state = (key, value = undefined) => {
        if ( !this.#states[key] && undefined !== value )
            this.#states[key] = ref(value);

        if ( undefined !== value )
            this.#states[key].value = value;

        return this.#states[key];
    }

    states = (object = false) => {
        if ( object ) {
            _.map(object, (value, key) => {
                this.state(key, value);
            });
            return this;
        }
        return this.#states;
    }

    /**
     * TODO reactive obj
     *
    reactive = (key, value = undefined) => {
        const parse = key.split(':');
        if (parse.length < 2)
            return undefined;

        if ( !this.#reactives[key] && undefined !== value ) {
            console.log('#reactives in : ', this.get(key));
        }
    }
    /**/

    useLayouts = () => {
        import('@/Layouts/~')
            .then((obj) => {
                this.layouts.value = obj.default;
                this.#ready.value = true;
            });
    }

    layout = (key) => {
        if ( !this.ready() )
            return false;

        const split = key.split(':');
        const name = split[0];
        const props = split[1];

        const layouts = toRaw(this.layouts.value);

        // if ( props !== undefined ) {
        //     const splitProps = props.split('.');
        //     const size = splitProps[0];
        //     const content = splitProps[1];
        // }
        return layouts[name];
    }

    /**
     * set Vue components as Ui
     * not business logic
     * @param {String} name - aka namespace for invoke
     * @param {Object} data - key/value object with aliases of Vue components
     * invoke method - this.view, example: ThisModuleName.view(name:alias);
     */
    ui = (name, data, replace = false) => {
        if ( undefined === this.#ui[name] || replace )
            this.#ui[name] = data;
        else
            this.#ui[name] = Object.assign(this.#ui[name], data);

        return this;
    }

    /**
     * set Vue components as Logic
     * not ui, css etc.
     */
    logic = (object, replace = false) => {
        this.#logic = object;
        return this;
    }

    /**
     * TODO Временное решение. Сделать полноценный класс импорте компонентов и js
     * TODO Продумать inject Модуля ( Module ). Криво работает при импорте
     * !!!!!!!!!!!!! inject работает нормально компоненты logic надо писать с учётом возможных переменных из других модулей, по возможности выносить все обработчики данных в index модуля
     *
     * Для динамических компонентов, подключаемых по ключу
     * Не работает, если исходные компоненты имеют одинаковый ключ
     * Компонент будет работать в контексте модуля, куда передаётся,
     * поэтому данные, используемые в нём тоже должны передаваться, если это другой модуль.
     * --- Corporate.set('data:service', Module.get('data:service'));
     * --- Corporate.viewUs('content', Module.view('services'));
     */
    import = (name, view) => {
        this.#logic[name] = view;
        return this;
    }

    viewList = (type = 'logic') => {
        return type === 'logic'
            ? this.#logic
            : this.#ui;
    }

    /**
     * get Vue logic & ui components
     */
    view = (key) => {
        const keys = key.split(':');
        const values = this.#ui[keys[0]];

        if ( keys.length === 2 && values )
            return values[keys[1]];

        return this.#logic[key];
    }

    use = (name, composable = null) => {
        if ( composable !== null && !this[name] )
            this[name] = composable;

        return this[name] !== undefined;
    }

    /**
     * TODO async include module
     */
//     include = async (as, module = null) => {
//         if ( module !== null && !this[as] ) {
//             const path = `../../Modules/${module}/`;
//
//             console.log('path in : ', path);
//             this[as] = import(path);
//         }
//
//         return this[as];
//     }

    instance = (name, inst = null, params = []) => {
        if ( inst !== null && !this[name] )
            this[name] = new inst(...params);

        return this[name];
    }

    /**
     * TODO Доработать, посмотреть как в принципе должно работать
     */
    bind = (name, inst = null, params = []) => {
        if ( inst !== null && !this[name] )
            this[name] = () => {return new inst(...params)};

        return this[name] !== undefined;
    }

    utils = (name, data) => {
        if ( undefined === this.#utils[name] )
            this.#utils[name] = data;
        else
            this.#utils[name] = Object.assign(this.#utils[name], data);

        return this.#utils[name];
    }

    unset = (name) => {
        delete this[name];
    }

    /**
     * DoubleDot&Dot notation args
     * Split DoubleDot first - this class variable (object)
     * Split dot - object nesting chain
     * for example: this.set('params:item.name', value)
     *      this.params.item.name;
     *      this.params = {item: {name: value}}
     */
    set = (key, value, toRef = false) => {
        const split = key.split(':');
        if ( undefined === split[1] ) {
            this[key] = toRef ? ref(value) : value;
            return this;
        }
        if ( undefined === this[split[0]] ) {
            this[split[0]] = toRef ? ref({}) : {};
        }
        const obj = toRef
            ? this[split[0]].value
            : this[split[0]];

        this.utils('dot').set(obj, split[1], value);
        return this;
    }

    get = (key, def = undefined) => {
        const split = key.split(':');

        if ( undefined === this[split[0]] )
            return def;

        if ( undefined === split[1] )
            return isRef(this[key]) ? this[key].value : this[key];

        const obj = isRef(this[split[0]])
            ? this[split[0]].value
            : this[split[0]];

        const result = this.utils('dot').get(obj, split[1]);
        return (undefined !== result)
            ? result
            : def;
    }

    /**
     * @param {String} key
     * @param {String, Object} value
     * @param {Boolean} deleteSource
     *
     * Если объект существует в контексте текущего класса и value строка - клонируем существующий объект с ключом value
     * Если объекта не существует в контексте текущего класса и value объект - клонируем value с ключом key
     */
    clone = (key, value, deleteSource = false) => {
        let obj = null;
        let name = null;
        if ( _.isPlainObject(this.get(key)) && _.isString(value) ) {
            name = value;
            obj = this.get(key);
        }
        else if (!_.isPlainObject(this.get(key)) && _.isPlainObject(value) ) {
            name = key;
            obj = value;
        }
        if ( !obj ) {
            console.warn(`Оbject by key ${name} cannot be cloned.`);
            return this;
        }

        this.set(name, JSON.parse(JSON.stringify(obj)));
        return this.get(name);
    }
}
