import { ref, watchEffect } from 'vue';
import { useWindowSize, useElementBounding, useElementSize } from '@vueuse/core';
import { useTextDirection } from '@vueuse/core';

/**
 * [TODO] add useTextDirection
 * Подумать, что делать если target undefined
 */
export function usePosition(trigger, target, align = 'right', offsetX = 0, offsetY = 0) {

    const window  = useWindowSize();
    const button  = useElementBounding(trigger);
    const content = target !== undefined
        ? useElementSize(target)
        : null;

    const left = ref(0);
    const top = ref(0);
    const style = ref('');

    const canUse = (side) => {

        if (!content)
            return true;

        return {
            left: button.left.value-offsetX > content.width.value,
            top: button.top.value-offsetY > content.height.value,
            right: (window.width.value-(button.right.value+offsetX)) > content.width.value,
            bottom: (window.height.value-(button.bottom.value+offsetY)) > content.height.value
        }[side]
    }

    const x = () => {
        let result = 0;
        switch(align) {
            case 'right':
                result = button.right.value+offsetX;
                if ( content && !canUse('right') && canUse('left') )
                    result = (button.left.value-content.width.value-offsetX);

                break;

            case 'left':
                result = (button.left.value-offsetX);

                if (!content)
                    break;

                result = (button.left.value-content.width.value-offsetX);
                if ( !canUse('left') && canUse('right') )
                    result = button.right.value+offsetX;

                break;

            case 'top':
            case 'bottom':

                if (!content)
                    break;

                result = canUse('left')
                    ? (button.right.value-content.width.value)
                    : button.left.value;

                break;
        }

        return result;
    }

    const y = () => {
        let result = 0;
        switch(align) {
            case 'top':

                if (!content) {
                    result = button.top.value;
                    break;
                }

                result = (button.top.value-content.height.value-offsetY);
                if ( !canUse('top') && canUse('bottom') )
                    result = button.bottom.value+offsetY;

                break;

            case 'bottom':
                if (!content) {
                    result = button.bottom.value;
                    break;
                }

                result = button.bottom.value+offsetY;
                if ( !canUse('bottom') && canUse('top') )
                    result = (button.top.value-content.height.value-offsetY);

                break;

            case 'left':
            case 'right':
                if (!content) {
                    result = button.top.value;
                    break;
                }

                result = canUse('bottom')
                    ? button.top.value-offsetY
                    : (button.bottom.value-content.height.value+offsetY);

                break;
        }

        return result;
    }

    watchEffect(() => {
        left.value = x();
        top.value = y();

        if ( left.value > 0 )
            style.value = `left:${left.value}px; top:${top.value}px;`;
    });


    return { left, top, style };
}
