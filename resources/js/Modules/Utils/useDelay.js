import { ref, watchEffect, onUnmount } from 'vue';

export function useDelay(triggerIn, delayIn, triggerOut = false, delayOut = false) {

    let timeout = null;
    const output = ref(false);

    if ( !triggerOut )
        triggerOut = triggerIn;

    if ( !delayOut )
        delayOut = delayIn;

    watchEffect(() => {
        if ( triggerIn.value === true && output.value === false ) {
            timeout = setTimeout(() => {
                output.value = true;
            }, delayIn);
        }
        else if (triggerOut.value === false && output.value === true) {
            timeout = setTimeout(() => {
                output.value = false;
            }, delayOut);
        }
    });

    onUnmount(() => {
        timeout = clearTimeout(timeout);
        output.value = false;
        // timeout = null;
    });

    return output;
}
