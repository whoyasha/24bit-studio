import _ from 'lodash';

export default {
    get: (object, key = false) => {

        if ( !key )
            return object;

        const split = key.split('.');

        if ( split.length === 1 )
            return object[key];

        let result = {};
        key.split('.').forEach((item, index) => {
            result = index === 0
                ? object[item]
                : result[item];
        });
        return result;
    },
    set: (object, key, value) => {

        console.log('object in DotDev : ', object, key, value);

        let result = {}
        let keys = key.split('.');

        if ( keys.length === 1 ) {
            object[key] = value;
            return true;
        }

        let reverse = JSON.parse(JSON.stringify(keys)).reverse();
        if ( !isNaN(reverse[0]) && object[keys[0]] !== undefined ) {
            object[keys[0]][reverse[1]].splice(Number(reverse[0]), 0, value);
            return true;
        }

        if ( object[keys[0]] === undefined )
            object[keys[0]] = {}

        result[reverse[0]] = value;
        reverse.forEach((key, index) => {
            if ( index > 0 ) {
                let item = {}
                    item[key] = result;
                result = item;
            }
        });
        object[keys[0]] = _.merge(object[keys[0]], result[keys[0]]);
        return true;
    },
    /**
     * TODO delete in dot
     */
    delete: (key) => {

    }
}
