import Main from './Main.vue';
import Cols2_12 from './Cols2_12.vue';
import Cols2_12_md from './Cols2_12_md.vue';
export default {
    'main': Main,
    'cols2/12': Cols2_12,
}
