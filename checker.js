import _ from 'lodash';
import { ref, isRef, reactive } from 'vue';
import { useCustomCheckers } from '../Custom';

export function useFormChecker(fields) {
	return new FormChecker(fields);
}

class FormChecker {

	#errors = {}
	#fields = {}
	#refs = {}

	constructor(fields) {
		this.fields(fields);
	}

	ref = (key) => {
		if ( !this.#refs[key] )
			this.#refs[key] = ref(null);

		return this.#refs[key];
	}

	fields = (fields = false) => {
		if ( fields ) {
			_.map(fields, (field) => {
				if ( !_.isPlainObject(field.validate) && !field.validate )
					return false;

				this.err(field.code, false);
				this.#fields[field.code] = field;
				this.ref(field.code);

				const checker = this.default(field);
				console.log('checker in : ', checker);
			});
		}
		return this.#fields;
	}

	field = {
		set: (key, value) => {
			this.#fields[key] = value;
		},
		unset: (key) => {
			delete this.#fields[key];
			return this;
		},
		get: (key) => {
			return this.#fields[key];
		}
	}

	default = (field) => {
		const ref = this.ref(field.code).value;
		console.log('ref in : ', ref);
//         const tag = ref.tagName.toLowerCase();
//
//         const fn = tag == 'select' || tag == 'textarea'
//             ? tag
//             : ref.getAttribute('type');
//
//         return field.form_type === fn && this[fn]
//             ? this[fn]
//             : false;
	}

	check = ( key, value ) => {

		const field = this.field.get(key);

		if ( !field )
			return false;

		if ( field.validate?.length && this.length(field.validate.length, value) ) {
			this.err(field.code, true);
			return true;
		}

//         const ref = this.ref(field.code);
//
//         console.log('ref in : ', ref.value);

		// let fn = null;
		// if ( fn = this.default(field) ) {
		//     fn.apply(this, [field, value]);
		// }
	}

	/**
	 * Если validate - объект, проверяем по max&min
	 * Если boolean - проверяем больше 1го
	 */
	length = (validate, value) => {

		const length = _.toString(value).length;

		if ( _.isBoolean(validate) )
			return length === 0;

		if ( validate.min && validate.max ) {
			return (length < validate.min || length > validate.max);
		}
		else if (validate.strict) {
			return length !== validate.strict;
		}
		else {
			return validate.min
				? length < validate.min
				: length > validate.max;
		}
		return false;
	}

	err = (key, value = undefined) => {
		if ( !this.#errors[key] )
			this.#errors[key] = ref(false);

		if ( this.#errors[key] && undefined !== value )
			this.#errors[key].value = value;

		return this.#errors[key];
	}

	errs = () => {
		_.map(this.#errors, (err, key) => {
			// console.log('key in : ', key, err);
		});
	}

	number = (field, value) => {
		// this.err(field.code, true);
		// console.log('value in : ', field, value);
	}

	tel = ( field, value ) => {
		const pattern = /(^8|7|\+7)((\d{10})|(\s\(\d{3}\)\s\d{3}\s\d{2}\s\d{2}))/;
		return pattern.test(value);
	}

	email = ( field, value ) => {
		const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([а-яёА_ЯЁa-zA-Z\-0-9]+\.)+[а-яёА_Яa-zA-Z]{2,}))$/;
		return pattern.test(value);
	}

	required = ( field, value ) => {

		switch ( field.field_type ) {

			case 'text' :
			case 'textarea' :
				if ( !field.data.multiple ) {
					if ( field.data.min === undefined )
						field.data.min = this.defaultMin;

					return value.length >= field.data.min;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( item.value.length < field.data.min )
							empty = Math.ceil(empty+1);
					});

					return empty !== field.data.fields.length;
				}
				break;

			case 'select' :
				return value.length > 0;
				break;

			case 'radio' :
			case 'checkbox' :
				if ( !field.data.multiple ) {
					return field.checked;
				} else {
					let empty = 0;
					field.data.fields.forEach( function(item) {
						if ( !item.checked && item.value.length )
							empty = Math.ceil(empty+1);
					});
					return empty !== field.data.fields.length;
				}
				break;
		}
	}

	getErrors = function() {
		return this.errors;
	}
}
