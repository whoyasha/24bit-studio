<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('corporate_forms', function (Blueprint $table) {
            $table->longText('message');
            $table->string('email');
            $table->string('phone');
            $table->mediumText('company');
            $table->mediumText('name');
            $table->timestamp('created_at');
            $table->bigIncrements('id');
        });
    }
    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('corporate_forms');
    }
};