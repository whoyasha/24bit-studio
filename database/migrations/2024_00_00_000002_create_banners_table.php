<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->boolean('active')->default(1);
            $table->mediumText('description')->nullable();
            $table->mediumText('button')->nullable();
            $table->mediumText('action')->nullable();
            $table->mediumText('href')->nullable();
            $table->mediumText('image')->nullable();
            $table->string('color')->nullable();
            $table->string('title')->nullable();
            $table->string('page')->nullable();
            $table->timestamps();
            $table->id();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('banners');
    }
};
