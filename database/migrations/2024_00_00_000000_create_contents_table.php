<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->string('source')->nullable();
            $table->string('category')->nullable();
            $table->string('show_in_line')->nullable();
            $table->dateTime('active_to')->nullable();
            $table->dateTime('active_from')->nullable();
            $table->boolean('active')->default(1);
            $table->json('gallery')->nullable();
            $table->mediumText('detailt_picture')->nullable();
            $table->mediumText('preview_picture')->nullable();
            $table->longText('content')->nullable();
            $table->longText('description');
            $table->string('slug')->unique()->index();
            $table->mediumText('page')->index();
            $table->mediumText('name')->index();
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
            $table->bigIncrements('id');
        });
    }
    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contents');
    }
};