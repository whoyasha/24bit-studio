<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->json('gallery')->nullable();
            $table->text('type');
            $table->mediumText('logo')->nullable();
            $table->mediumText('preview_picture')->nullable();
            $table->longText('content')->nullable();
            $table->longText('description');
            $table->string('slug')->unique()->index();
            $table->mediumText('name')->index();
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
            $table->bigIncrements('id');
        });
    }
    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};