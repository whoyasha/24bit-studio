<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use App\Models\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения контактов");

        if (Contact::count() > 0)
        {
            error("Таблица не пустая, наполнение остановлено");
        }
         else
        {
            $config = config('configurator')->getContent();
            $content = $config->get('contacts');
            foreach ( $content as $id => $item ) {
                $content[$id]['created_at'] = now()->format('Y-m-d H:i:s');
                $content[$id]['updated_at'] = now()->format('Y-m-d H:i:s');
            }

            DB::table("contacts")->insert($content);

           warning('Наполнение контактов прошло успешно');
        }
    }
}
