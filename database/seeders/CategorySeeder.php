<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения категорий");

        if (Category::count() > 0)
        {
            error("Таблица не пустая, наполнение остановлено");
        }
         else
        {
            DB::table("categories")->insert([
                [
                    'id' => 1,
                    'active' => true,
                    'slug' => 'company_news',
                    'name' => 'Новости компании',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ],
                [
                    'id' => 2,
                    'active' => true,
                    'slug' => 'news_1c',
                    'name' => 'Новости 1C',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ],
                [
                    'id' => 3,
                    'active' => true,
                    'slug' => 'chestniy_znak',
                    'name' => 'Честный знак',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ],
                [
                    'id' => 4,
                    'active' => true,
                    'slug' => 'products_update',
                    'name' => 'Обновления продуктов',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ],
                [
                    'id' => 5,
                    'active' => true,
                    'slug' => 'legal',
                    'name' => 'Изменения законодательства',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ],
                [
                    'id' => 6,
                    'active' => true,
                    'slug' => 'projects',
                    'name' => 'Реализованные проекты',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ]
            ]);

           warning('Наполнение категорий прошло успешно');
        }
    }
}
