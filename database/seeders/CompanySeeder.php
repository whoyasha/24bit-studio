<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения о компании");

        if (Company::count() > 0)
        {
            error("Таблица не пустая, наполнение остановлено");
        }
         else
        {
            DB::table("companies")->insert([
                [
                    'active' => true,
                    'title' => 'О компании',
                    'description' => '<p>Мы занимаемся автоматизацией бизнес-процессов компаний с 2015 года и делаем удобнее работу с данными. В процессе работы над проектами мы используем гибкие методологии разработки программного обеспечения, осуществляем выбор оптимальных технологических стеков для каждого проекта.</p>
                    <p>Наша миссия – это разработка, внедрение и сопровождение информационных систем и решений, помогающих бизнесу развиваться и уверенно смотреть в будущее.</p>
',
                    'created_at' => now()->format('Y-m-d H:i:s'),
                    'updated_at' => now()->format('Y-m-d H:i:s')
                ]
            ]);

           warning('Наполнение о компании прошло успешно');
        }
    }
}
