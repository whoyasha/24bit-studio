<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use App\Models\Content;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения «contents»");

        if (Content::count() > 0) {
            error("Таблица не пустая, наполнение остановлено");
        }
        else {
            $config = config('configurator')->getContent();
            $content = $config->get('contents');
            foreach ( $content as $id => $item ) {
                $test = collect($item);
                $content[$id]['slug'] = Str::slug($item['name']).'-'.Str::random(7);
                if ( $test->has('content') ) {
                    $content[$id]['content'] = $item['content'];
                }
                $content[$id]['created_at'] = now()->format('Y-m-d H:i:s');
                $content[$id]['updated_at'] = now()->format('Y-m-d H:i:s');
            }

            DB::table("contents")->insert($content);

            warning('Наполнение «contents» прошло успешно');
        }
    }
}
