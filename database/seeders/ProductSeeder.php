<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use Illuminate\Support\Str;

use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения продукты и услуги");

        if (Product::count() > 0)
        {
            error("Таблица не пустая, наполнение остановлено");
        }
         else
        {
            $config = config('configurator')->getContent();
            $content = $config->get('products');
            foreach ( $content as $id => $item ) {
                $test = collect($item);
                $content[$id]['slug'] = Str::slug($item['name']);
                if ( $test->has('content') ) {
                    $content[$id]['content'] = $item['content'];
                }
                $content[$id]['created_at'] = now()->format('Y-m-d H:i:s');
                $content[$id]['updated_at'] = now()->format('Y-m-d H:i:s');
            }

            DB::table("products")->insert($content);

           warning('Наполнение продукты и услуги прошло успешно');
        }
    }
}
