<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use function Laravel\Prompts\warning;
use function Laravel\Prompts\error;

use App\Models\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        warning("Старт наполнения «banners»");

        if (Banner::count() > 0) {
            error("Таблица не пустая, наполнение остановлено");
        }
        else {
            $config = config('configurator')->getContent();
            $banners = $config->get('banners');
            foreach ( $banners as $id => $item ) {
                $banners[$id]['created_at'] = now()->format('Y-m-d H:i:s');
                $banners[$id]['updated_at'] = now()->format('Y-m-d H:i:s');
            }

            DB::table("banners")->insert($banners);

            warning('Наполнение «banners» прошло успешно');
        }
    }
}
