<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;
    
    protected $fillable = ['slug', 'active'];
    
    protected $casts = ['active_from' => 'datetime:d.m.Y', 'active_to' => 'datetime:d.m.Y'];
    
    protected $table = 'contents';
}