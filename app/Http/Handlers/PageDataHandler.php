<?php

namespace App\Http\Handlers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use App\Http\Handlers\LogoHandler;
use App\Models\Contact;
use App\Models\Content;
use App\Models\Banner;
use App\Models\Category;

class PageDataHandler
{
    public $config;

    public $contacts;

    public $content;

    public array $lineCategories;

    public string $orderLine = 'active_from';

    public int $itemsLimit = 10;

    public string $articlesOrder = 'active_from';

    public int $articlesLimit = 6;

    /**
     * @param array $params
     * - articles->limit
     * - articles->order
     * - articles->use_top
     * - articles->all
     */
    public function __invoke(array $params = null)
    {
        $this->config = config('configurator');

        $this->contacts = new Contact;
        $this->content = new Content;

        $banners = New Banner;

        $configContent = $this->config->getContent();
        $logoHandler = app()->make(LogoHandler::class);

        $mainContacts = [
            'phone' => $this->contacts->where('type', 'phone')->first(),
            'email' => $this->contacts->where('type', 'email')->first(),
            'support_phone' =>  $this->contacts->where('type', 'support_phone')->first(),
            'sale'  => $this->contacts->where('type', 'sale_email')->first()
        ];

        $lineCategories = $this->config->get('line_categories')['content'];
        $this->lineCategories = collect($lineCategories)->keys()->toArray();
        $items = $this->content->orderByDesc($this->orderLine)->limit($this->itemsLimit)->whereIn('show_in_line', $this->lineCategories);
        $itemsCount = $items->count();

        $current = Route::currentRouteName();
        $page = Str::of($current)->replace(['page.', '.index', '.detail'],'')->value();

        $result = [
            'logo'            => $logoHandler(),
            'current'         => $current,
            'page'            => $page,
            'menu'            => collect($configContent->get('menu'))->where('active', true),
            'contacts'        => $mainContacts,
            // 'banners'         => $banners->where('page', $page)->get(),
            'banners'         => $banners->where('page', 'main')->get(),
            'line' => [
                'filter' => [
                    'fields' => [
                        'categories' => $lineCategories
                    ],
                    'data' => [
                        'countTotal' => $itemsCount,
                        'pageNum'    => 1,
                        'pageCount'  => ceil($itemsCount/(int) $this->itemsLimit),
                        'limit'      => $this->itemsLimit,
                        'sort'       => $this->orderLine
                    ],
                ],
                'items' => $items->get()
            ],
        ];

        if ( isset($params['articles']) ) {

            $result['articles'] = [];

            $articlesParams = collect($params['articles']);

            if ( $articlesParams->has('limit') )
                $this->articlesLimit = $articlesParams->get('limit');

            if ( $articlesParams->has('order') )
                $this->articlesOrder = $articlesParams->get('order');

            $categories = new Category;
            $result['articles']['filter'] = ['categories' => $categories->all()->flatMap(function($item) { return [$item['slug'] => $item['name']]; })];

            if ( $articlesParams->has('use_top') && $articlesParams->get('use_top') ) {
                $result['articles']['top'] = $this->content->where('show_in_line', 'top')->first();
            }

            $articles = $this->content->orderByDesc($this->articlesOrder)
                    ->limit($this->articlesLimit);

            /**
             * Фильтруем по полю page
             */
            if ( !$articlesParams->has('all') )
                $articles->where('page', $page);

            $result['articles']['items'] = $articles->get();

            $result['articles']['params'] = [
                'countTotal'   => $this->articlesLimit,
                'pageNum'      => 1,
                'pageCount'    => 1,
                'desktopLimit' => $this->articlesLimit
            ];
        }

        return $result;
    }
}
