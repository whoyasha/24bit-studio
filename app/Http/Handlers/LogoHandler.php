<?php

namespace App\Http\Handlers;

use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;

class LogoHandler
{
    protected Collection $params;

    public function __invoke(array $params = null)
    {
        $content = config('configurator')->getContent();
        if ( $content->has('logo') )
            $params = $content->get('logo');

        $this->params = collect($params);

        if ( $force = $this->getForce() )
            return $force->first();

        return $this->getByDate()->first();
    }

    protected function getForce(): Collection|bool
    {
        $force = $this->params->get('force');
        $items = $this->params->get('items');
        return (strlen($force) > 0 && $items[$force]['active'])
            ? collect([$force => $items[$force]])
            : false;
    }

    protected function getByDate()
    {
        $today = today();
        $items = collect($this->params->get('items'));

        $byDate = $items->map(function($item) use ($today) {
            if ( !empty($item['period']) &&  is_array($item['period']) ) {
                $period = $item['period'];
                $year = $today->format('Y');
                $from = new Carbon($year.'/'.$period['from']);
                $to = new Carbon($year.'/'.$period['to']);

                if ( $to < $from )
                    $to = $to->addYear();

                if ( $item['active'] && $today > $from && $today <= $to )
                    return $item;
            }
            else {
                return $item;
            }
        });

        $result = $byDate->whereNotIn('resource', [""]);
        if ( $result->count() > 1 ) {
            $result->forget('default');
        }

        return $result;
    }
}
