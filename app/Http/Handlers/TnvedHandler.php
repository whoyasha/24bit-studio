<?php

namespace App\Http\Handlers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TnvedHandler extends Controller
{
    //: RedirectResponse
    public function index(Request $request)
    {
        $result = [
            'input' => $request->input(),
            'success' => true,
            'data' => [
                [
                    'Items' => [
                        [
                            'Item_code' => '64',
                            'Item_description' => 'обувь, гетры и аналогичные изделия; их детали',
                            'Item_select' => false
                        ],
                        [
                            'Item_code' => '6404',
                            'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                            'Item_select' => true
                        ],
                        // [
                        //     'Item_code' => '640445',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => false
                        // ],
                        // [
                        //     'Item_code' => '64044568',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => true
                        // ],
                        // [
                        //     'Item_code' => '640445',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => false
                        // ],
                        // [
                        //     'Item_code' => '6404456',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => true
                        // ],
                        // [
                        //     'Item_code' => '640445',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => false
                        // ],
                        // [
                        //     'Item_code' => '6404456',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => true
                        // ],
                        // [
                        //     'Item_code' => '640445',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => false
                        // ],
                        // [
                        //     'Item_code' => '6404456',
                        //     'Item_description' => 'обувь с подошвой из резины, пластмассы, натуральной или композиционной кожи и с верхом из текстильных материалов',
                        //     'Item_select' => true
                        // ]
                    ],
                    'marked_flag' => true,
                    'marked_description' => 'Данная товарная группа подлежит обязательной маркировке'
                ]
            ]
        ];
        return response()->json($result);
    }
}
