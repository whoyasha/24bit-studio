<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Http\Handlers\PageDataHandler;

class SupportController extends Controller
{
    public function index(PageDataHandler $page)
    {
        $pageData = $page(['articles' => true]);

        $models = config('configurator')->getModels();

        $supportContactPhone = $page->contacts->where('active', true)->where('type', 'support_phone')->first();
        $contactPhone = $page->contacts->where('active', true)->where('type', 'phone')->first();
        $supportContactEmail = $page->contacts->where('active', true)->where('type', 'support_email')->first();

        $pageData['form'] = [
            'description' => 'Для того, чтобы заказать услугу или задать вопрос на по нашим продуктам и сервисам воспользуйтесь этой формой обратной связи. Заполните контактную информацию и задайте ваш вопрос в свободной форме',
            'fields' => $models['supports']
        ];

        $pageData['content']['contacts'] = [
            'title' => 'Контакты линии технической поддержки',
            'description' => 'Наши специалисты оперативно ответят на вопросы пользователей и помогут в решении различных проблем для наших программных продуктов и приобретенных у нас конфигураций 1С:Предприятие.»',
            'items' => [
                'support_phone' => $supportContactPhone,
                'phone' => $contactPhone,
                'email' => $supportContactEmail
            ]
        ];
        $pageData['content']['grafik'] = [
            'items' => [
                'ПН' => '09:00 – 18:00',
                'ВТ' => '09:00 – 18:00',
                'СР' => '09:00 – 18:00',
                'ЧТ' => '09:00 – 18:00',
                'ПТ' => '09:00 – 18:00',
                'СБ-ВС' => 'ВЫХОДНОЙ'
            ],
            'description' => 'Поддержка и консультирование в иное время осуществляется только по договору SLI, по оговоренным в договоре способам связи.'
        ];
        $pageData['content']['lk'] = [
            'title' => 'Личный кабинет клиента',
            'image' => 'boy-and-laptop',
            'description' => 'Для зарегистрированных клиентов и клиентов по договорам SLI поддержка осуществляется с помощью инструментов личного кабинета.',
            'features' => [
                'title' => 'В личном кабинете вы сможете:',
                'items' => [
                    'Посмотреть приобретенные вами программные продукты и получить консультации к ним.',
                    'Посмотреть подключенные услуги сопровождения или облачные сервисы.',
                    'Произвести продление и оплату подключенных облачных сервисов или услуг, историю взаиморасчетов.',
                    'Оформить заявку на обслуживание (при действующем договоре SLI).'
                ]
            ],
            'link' => [
                'title' => 'Перейти в личный кабинет',
                'href' => 'https://lk.studio-24bit.cloud'
            ]
        ];
        $pageData['content']['info'] = [
            'title' => 'Полезная информация',
            'items' => [
                [
                    'title' => 'Линия консультаций 1С:КП (ИТС)',
                    'description' => 'Специалисты линии консультации ответят на Ваши вопросы о работе и типовых конфигураций и отраслевых решений компании «Студия-24бит». <a href="mailto:webits@studio-24bit.ru">webits@studio-24bit.ru</a> (для получения консультации требуется действующий договор 1С:КП). ',
                    'image' => '/images/tabler/dark/mobile-computer.png',
                    'icon' => 'consalt-mess'
                ],
                [
                    'title' => 'Консалтинговые услуги',
                    'description' => 'Наши специалисты проконсультируют вас в области бухгалтерского учета, обязательной маркировки продукции, электронного документооборота и ВЭД. <a href="mailto:consulting@studio-24bit.ru">consulting@studio-24bit.ru</a> (для получения консультации требуется действующий договор SLI).',
                    'image' => '/images/tabler/dark/icons.png',
                    'icon' => 'consult-service'
                ],
                [
                    'title' => 'База знаний и FAQ',
                    'description' => 'Здесь Вы можете найти <a href="https://help.studio-24bit.cloud">ответы</a> на часто задаваемые вопросы по работе с продуктами и сервисами компании «Студия-24бит» и «1С: Предприятие».',
                    'image' => '/images/tabler/dark/neutral-info.png',
                    'icon' => 'knowelege'
                ],
                [
                    'title' => 'Письмо в отдел качества',
                    'description' => '     База знаний и FAQ.
                    Здесь Вы можете найти ответы на часто задаваемые вопросы по работе с продуктами и сервисами компании «Студия-24бит» и «1С: Предприятие».		 	Письмо в отдел качества.
                    При желании улучшить качество сервисов и услуг «Студия-24бит», Вы можете направить обращение в отдел качества компании.
                    <a href="mainto:quality@studio-24bit.ru">quality@studio-24bit.ru</a>',
                    'image' => '/images/tabler/dark/email.png',
                    'icon' => 'mail-star'
                ]
            ]
        ];



        $pageData['showPageTitle'] = true;
        $pageData['pagetitle'] = 'Поддержка';
        $pageData['showBreadcrumbs'] = true;
        $pageData['breadcrumbs'] = $this->chain();

        return Inertia::render('Support', [
           'pageData' => $pageData,
        ]);
    }

    protected function chain()
    {
       return [
            0 => ['url' => '/', 'title' => 'Главная'],
            1 => ['url' => null, 'title' => 'Поддержка']
        ];
    }

    public function store(Request $request)
    {
        $request->validate([
            'company' => ['required', 'max:50'],
            'name'    => ['required', 'max:50'],
            'email'   => ['required', 'max:50', 'email'],
            'subject' => ['required'],
            'message' => ['required', 'max:255']
        ]);

//         $data = [
//             'name'    => $request->name,
//             'email'   => $request->email,
//             'phone'   => $request->phone,
//             'subject' => $request->subject,
//             'message' => $request->message
//         ];
//
//         $result = Support::create($data);
        // return back()->with([
        //     'message' => 'Ваше сообщение успешно отправлено!',
        // ]);
        return redirect()->route('page.support', ['success' => 'Ваше сообщение успешно отправлено!']);
        // return Redirect::route('page.support', $user);
        // return response()->json(['success' => 'Ваше сообщение успешно отправлено!']);

        // return Inertia::render('Welcome', [
        //    'seccess' => 'Ваше сообщение успешно отправлено!',
        // ]);
    }
}
