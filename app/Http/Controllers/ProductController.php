<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use Illuminate\Support\Str;

use App\Models\Product;
use App\Http\Handlers\PageDataHandler;



class ProductController extends Controller
{
    protected Request $request;

    protected array $data = [];

    public function index(Request $request, PageDataHandler $page) {

        $this->data = $page(['articles' => ['limit' => 4]]);
        $this->request = $request;

        $models = config('configurator')->getModels();
        $this->data['corporate_form'] = $models['corporate_form'];

        $products = new Product;
        $productItems = $products->all();

        $render = 'Products/Index';

        if ( $this->subpage() === 'main' ) {
            $this->data['listParams'] = [
                'countTotal'   => $products->count(),
                'pageNum'      => 1,
                'pageCount'    => 1,
                'sort'         => null,
                'desktopLimit' => 0
            ];
            $this->data['products'] = $productItems->groupBy('type');
        }
        elseif ( $this->subpage() === 'detail' ) {
            $this->data['detail'] = $products->where('slug', $this->detail())->first();
            unset($this->data['articleItems'], $this->data['articleParams']);
            $render = 'Products/Detail';
            if ( !$this->data['detail'] )
                abort(404);
        }
        else {
            abort(404);
        }

        $this->data['showPageTitle'] = true;
        $this->data['pagetitle'] =  $this->subpage() === 'main'
            ? 'Продукты и услуги'
            : $this->data['detail']['name'];

        $this->data['showBreadcrumbs'] = true;
        $this->data['breadcrumbs'] = $this->chain();

        return Inertia::render($render, [
           'pageData' => $this->data,
        ]);
    }

    protected function detail() {
        return $this->request->segment(2);
    }

    protected function subpage() {
       return $this->detail() ? 'detail' : 'main';
    }

    protected function chain() {
        $chain = [
            0 => ['url' => '/', 'title' => 'Главная'],
            1 => ['url' => null, 'title' => 'Продукты и услуги']
        ];

        if ( $this->subpage() === 'detail' ) {
            $chain[1]['url'] = '/products';
            $title = $this->data['detail']->getAttribute('name');
            $chain[] = ['url' => null, 'title' => $title];
        }

        return $chain;
    }
}
