<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Content;

class NewsLineController extends Controller
{
    protected $sort = 'active_from';
    protected $limit = 10;
    protected $page = 1;

    public function index(Request $request) {

        $params = [
            'sort'     => $request->get('sort') ? $request->get('sort') : $this->sort,
            'limit'    => $request->get('limit') ? $request->get('limit') : $this->limit,
            'page'     => $request->get('page') ? $request->get('page') : $this->page,
            'category' => $request->get('category') ? [$request->get('category')] : ['info', 'warning', 'danger']
        ];

        $data = new Content;
        $count = $data->whereIn('show_in_line', $params['category'])->count();

        $offset = ($params['limit']*$params['page']-$params['limit']);
        $items = $data->orderByDesc($params['sort'])
            ->whereIn('show_in_line', $params['category'])
            ->offset($offset)
            ->limit($params['limit'])
            ->get();

        $result = [
            'params' => $params,
            'countTotal' => $count,
            'pageCount' => ceil($count/(int) $params['limit']),
            'pageNum' => $params['page'],
            'desktopLimit' => $params['limit'],
            'sort'    => $params['sort'],
            'items' => $items,
        ];

        return response()->json($result);
    }
}
