<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Content;

class ArticlesApiController extends Controller
{
    protected $sort = 'active_from';

    protected $limit = 10;

    protected $page = 1;

    protected $categories = [
        'company_news',
        'news_1c',
        'chestniy_znak',
        'products_update',
        'legal',
        'projects'
    ];

    public function index(Request $request) {

        $params = [
            'sort'     => $request->get('sort'),
            'limit'    => $request->get('limit'),
            'page'     => $request->get('page'),
            'category' => $request->get('category') ? [$request->get('category')] : $this->categories
        ];

        $data = new Content;
        $content = $data->where('show_in_line', '!=', 'top')
                    ->whereIn('category', $params['category']);

        $count = $content->count();
        $offset = ($params['limit']*$params['page']-$params['limit']);

        $content->orderByDesc($params['sort'])
            ->offset($offset)
            ->limit($params['limit']);

        $items = $content->get();

        $result = [
            'countTotal' => $count,
            'pageCount' => ceil($count/(int) $params['limit']),
            'pageNum' => $params['page'],
            'desktopLimit' => $params['limit'],
            'sort'    => $params['sort'],
            'items' => $items,
        ];

        return response()->json($result);
    }
}
