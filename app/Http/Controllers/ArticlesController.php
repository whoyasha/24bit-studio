<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use Illuminate\Support\Str;

use App\Models\Category;
use App\Http\Handlers\PageDataHandler;

class ArticlesController extends Controller
{
    protected Request $request;

    protected array $data = [];

    protected int $articlesLimit = 9;

    protected array $articlesCategories = [
        'company_news',
        'news_1c',
        'chestniy_znak',
        'products_update',
        'legal',
        'projects'
    ];

    protected string $orderArticles = 'active_from';

    protected string $orderLine = 'active_from';

    protected array $article = [];

    public function index(Request $request, PageDataHandler $page) {

        $this->data = $page();
        $this->request = $request;

        $categories = new Category;
        $categoriesItems= $categories->all();
        $this->data['content_categories'] = $categoriesItems->flatMap(function($item) {
            return [$item['slug'] => $item['name']];
        });

        $content = $page->content;

        $category = $this->category()
            ? [$this->category()]
            : $this->articlesCategories;

        $topArticle = null;
        $firts = $content->where('show_in_line', 'top')->first();
        if ( in_array($firts->category, $category) ) {
            $topArticle = $firts;
        }

        $article = null;
        $artiicleCount = 0;

        $view = 'Articles/Index';
        if ( $this->subpage() === 'main' || ($this->subpage() === 'category' && $this->categoryExists()) ) {
            $articles = $content->orderByDesc($this->orderArticles)
                ->where('show_in_line', '!=', 'top')
                ->whereIn('category', $category);

            $artiicleCount = $articles->count();
            $articles->limit($this->articlesLimit);

            if ($this->subpage() === 'category' && $this->categoryExists())
                $view = 'Articles/Category';
        }
        else if ( $this->subpage() === 'detail' ) {
            $article = $content->where('slug', $this->detail())->first();
            if ( !$article )
                abort(404);

            $view = 'Articles/Detail';
        }
        else {
            abort(404);
        }

        if ( $artiicleCount > 0 ) {
            $this->data['articleParams'] = [
                'countTotal'   => $artiicleCount,
                'pageNum'      => 1,
                'pageCount'    => ceil($artiicleCount/$this->articlesLimit),
                'sort'         => $this->orderArticles,
                'desktopLimit' => $this->articlesLimit
            ];
            if ( null !== $topArticle ) {
                $this->data['topArticle'] = $topArticle;
            }
            $this->data['articleItems'] = $articles->get();
        }

        if ( null !== $article ) {
            $this->data['detail'] = $article;
        }

        $this->data['showPageTitle'] = true;
        $this->data['pagetitle'] = 'Публикации';

        if ($this->subpage() === 'category' && $this->categoryExists())
            $this->data['pagetitle'] = 'Публикации: '.$this->data['content_categories'][$this->category()];

        if ($this->subpage() === 'detail')
            $this->data['pagetitle'] = $this->data['detail']['name'];

        $this->data['showBreadcrumbs'] = true;
        $this->data['breadcrumbs'] = $this->chain();

        return Inertia::render($view, [
           'pageData' => $this->data,
        ]);
    }

    protected function category()
    {
        return $this->request->segment(2);
    }

    protected function categoryExists()
    {
        return in_array($this->category(), $this->articlesCategories);
    }

    protected function detail()
    {
        return $this->request->segment(3);
    }

    protected function subpage() {
        if ( ( !$this->category() && !$this->detail() ) )
            return 'main';

        elseif ( ( $this->category() && !$this->detail() ) )
            return 'category';

        elseif ( ( $this->category() && $this->detail() ) )
            return 'detail';
    }

    protected function chain()
    {
        $url = ( $this->subpage() !== 'category' && $this->subpage() !== 'detail' )
            ? null
            : route('page.articles', [], false);

        $chain = [
            0 => ['url' => '/', 'title' => 'Главная'],
            1 => ['url' => $url, 'title' => 'Публикации']
        ];

        if ( $this->category() === 'projects' ) {
            $chain[1] =  ['url' => '/about', 'title' => 'О компании'];
        }

        if ( $this->subpage() === 'category' || $this->subpage() === 'detail' ) {
            $url = null;
            if ( $this->subpage() === 'detail' )
                $url = route('page.articles.category', ['category' => $this->category()], false);

            $chain[] = ['url' => $url, 'title' => $this->data['content_categories'][$this->category()]];
        }

        if ( $this->subpage() === 'detail' ) {
            $title = $this->data['detail']->getAttribute('name');
            $title = Str::of($title)->substr(0, 40)->finish('...')->value();
            $chain[] = ['url' => null, 'title' => $title];
        }

        return $chain;
    }
}
