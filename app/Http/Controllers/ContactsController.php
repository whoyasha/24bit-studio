<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

use App\Http\Handlers\PageDataHandler;

class ContactsController extends Controller
{
    public function index(PageDataHandler $page)
    {
        $pageData = $page();
        $contactsItems = $page->contacts->where('active', true)->get();
        $pageData['detail'] = [
            'requisite' => $contactsItems->groupBy('type')->get('requisite'),
            'address' => $contactsItems->groupBy('type')->get('address')->first(),
            // 'support' => [
            //     'phone' => $contactsItems->where('type', 'support_phone')->first(),
            //     'email' => $contactsItems->where('type', 'support_email')->first()
            // ]
        ];

        $pageData['showPageTitle'] = true;
        $pageData['pagetitle'] = 'Контакты';

        $pageData['showBreadcrumbs'] = true;
        $pageData['breadcrumbs'] = $this->chain();

        return Inertia::render('Contacts', [
           'pageData' => $pageData
        ]);
    }

    protected function chain()
    {
       return [
            0 => ['url' => '/', 'title' => 'Главная'],
            1 => ['url' => null, 'title' => 'Контакты']
        ];
    }
}
