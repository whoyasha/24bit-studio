<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

use App\Http\Handlers\PageDataHandler;

use App\Models\Company;

class AboutController extends Controller
{
    public function index(PageDataHandler $page)
    {
        $pageData = $page(['articles' => ['limit' => 4]]);
        $pageData['chain'] = $this->chain();

        $about = new Company;
        $pageData['content'] = $about->first();

        $pageData['articles_view'] = 'project';

        $pageData['showPageTitle'] = true;
        $pageData['pagetitle'] = 'О компании';

        $pageData['showBreadcrumbs'] = true;
        $pageData['breadcrumbs'] = $this->chain();

        return Inertia::render('About', [
           'pageData' => $pageData
        ]);
    }

    protected function chain()
    {
       return [
            0 => ['url' => '/', 'title' => 'Главная'],
            1 => ['url' => null, 'title' => 'О компании']
        ];
    }
}
