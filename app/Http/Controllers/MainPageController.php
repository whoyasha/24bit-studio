<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Inertia\Inertia;

use App\Http\Handlers\PageDataHandler;

class MainPageController extends Controller
{
    public function index(PageDataHandler $page)
    {
        $pageData = $page(['articles' => ['limit' => 8, 'use_top' => true]]);
        $models = config('configurator')->getModels();

        $pageData['corporate_form'] = $models['corporate_form'];

        $pageData['showPageTitle'] = false;

        return Inertia::render('MainPage', [
           'pageData' => $pageData,
        ]);
    }
}
