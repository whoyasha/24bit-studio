<?php

return [
    'cmd:package' => [
        'title'         => 'Laravel package installer',
        'description'   => 'Laravel package installer',
        'namespace'       => 'cmd:package',
        'commands_path' => '/vendor/whoyasha/cmdmenu/src/PackagesInstaller'
    ],
    'cmd:cache' => [
        'title'         => 'Work with cache',
        'description'   => 'Work with cache',
        'namespace'       => 'cmd:package',
        'commands_path' => '/vendor/whoyasha/cmdmenu/src/Cache'
    ],
    'cmd:extend' => [
        'title'         => 'Extend CmdMenu',
        'description'   => 'Extend CmdMenu',
        'namespace'       => 'cmd:extend',
        'commands_path' => '/vendor/whoyasha/cmdmenu/src/Extend'
    ]
];
